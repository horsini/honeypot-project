#!/usr/bin/env python3

import os
import time

"""
    _________________________________________________________________________________________________________

    Ce script a pour but de monitorer les logs de mongodb et d'envoyer une notification lorsqu'une authentification
    réussie est détectée.

    Il se base sur le fichier de logs de mongodb par défaut et sur un pattern précisé via la variable pattern.

    Auteur(s) : [Marius Le Douarin]
    _________________________________________________________________________________________________________
"""

# Logs mongodb
log_file = "/var/log/mongodb/mongod.log"

# Alerte visée
pattern = "\"msg\":\"Authentication succeeded\""

auth_alert_tracking_file = "/var/log/mongodb/recent_auth_alerts.txt"

current_timestamp = int(time.time())

def send_notification(alert_type, most_recent_alert):
    '''
        Envoie une notification de détection d'intrusion

        args:
            alert_type : Le type d'alerte à envoyer
            most_recent_alert : La dernière alerte détectée
    '''
    # TODO
    print(f"Envoie d'une notification pour : {alert_type}...")

def main():
    if os.path.exists(log_file):
        with open(log_file, "r") as f:
            log_contents = f.read()

            if pattern in log_contents:
                most_recent_alert = max(
                    (line for line in log_contents.splitlines() if pattern in line),
                    key=lambda line: line.split(" ", 2)[0]
                )

                if os.path.exists(auth_alert_tracking_file):
                    with open(auth_alert_tracking_file, "r") as af:
                        last_alert = af.read().strip()

                    if most_recent_alert != last_alert:
                        send_notification("Authentication succeeded", most_recent_alert)

                        with open(auth_alert_tracking_file, "w") as af:
                            af.write(most_recent_alert)
                    else:
                        print("Pas de nouvelle alerte (Authentication succeeded)")
                else:
                    send_notification("Authentication succeeded", most_recent_alert)
                    with open(auth_alert_tracking_file, "w") as af:
                        af.write(most_recent_alert)
            else:
                print("Aucune ligne (Authentication succeeded) détectée")
    else:
        print(f"Log file not found: {log_file}")

if __name__ == "__main__":
    main()