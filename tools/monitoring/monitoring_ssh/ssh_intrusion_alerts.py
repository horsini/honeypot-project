#!/usr/bin/env python3

import os
import time

"""
    _________________________________________________________________________________________________________

    Ce script a pour but de monitorer les logs SSH et d'envoyer une notification lorsqu'une authentification
    réussie est détectée.

    Il se base sur le fichier de logs SSH auth.log et sur un pattern précisé via la variable pattern.

    Auteur(s) : [Marius Le Douarin]
    _________________________________________________________________________________________________________
"""

# SSH logs
log_file = "/var/log/auth.log"

# Alerte visée
pattern = "Accepted password"

auth_alert_tracking_file = "/var/log/ssh/recent_auth_alerts.txt"

current_timestamp = int(time.time())

def send_notification(alert_type, most_recent_alert):
    '''
        Envoie une notification de détection d'intrusion

        args:
            alert_type : Le type d'alerte à envoyer
            most_recent_alert : La dernière alerte détectée
    '''
    # TODO
    print(f"Envoie d'une notification pour : {alert_type}...")

def main():
    if os.path.exists(log_file):
        with open(log_file, "r") as f:
            log_lines = f.readlines()

        relevant_lines = [line for line in log_lines if pattern in line]
        if relevant_lines:
            most_recent_alert = relevant_lines[-1]

            if os.path.exists(auth_alert_tracking_file):
                with open(auth_alert_tracking_file, "r") as af:
                    last_alert = af.read().strip()

                if most_recent_alert.strip() != last_alert.strip():
                    send_notification("SSH Authentication succeeded", most_recent_alert)

                    with open(auth_alert_tracking_file, "w") as af:
                        af.write(most_recent_alert)
                else:
                    print("Pas de nouvelle alerte (SSH Authentication succeeded)")
            else:
                send_notification("SSH Authentication succeeded", most_recent_alert)
                with open(auth_alert_tracking_file, "w") as af:
                    af.write(most_recent_alert)
        else:
            print("Aucune ligne (SSH Authentication succeeded) détectée")
    else:
        print(f"Log file not found: {log_file}")

if __name__ == "__main__":
    main()
