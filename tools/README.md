# Utiliser argus via Docker / Podman

## Build une image si elle n'est pas construite
```bash
podman build -t argus .
```

```bash
docker build -t argus .
```

## Utiliser l'image
```bash
podman run --rm -v ./PATH_DIR_PCAP/:/logdir argus /logdir
```

```bash
docker run --rm -v ./PATH_DIR_PCAP/:/logdir argus /logdir
```

## PCAP corrompus
Parfois certains PCAP sont coupés en plein milieu de communication et certains paquets sont incomplets.
Parfois cela gène argus, parfois non.
Si c'est le cas, il est possible d'utiliser le logiciel **pcapfix**; cela résoud le problème dans la plupart des cas.

# Commentaires de fin de stage de Marius

⚠️ Il s'est avéré, après plusieurs tests, que le script [pcap2csv.py](tools/flow/pcap2csv.py) fonctionnait comme nous le souhaitions sur une VM dont l'image était ubuntu/focal64 qui correspond à une Ubuntu 20.04 (c'est avec celle-ci que j'ai pu correctement effectuer la récupération des logs pour Hélène le 27/08/23). Cependant lorsque nous avons voulu le tester sur marmelab après la dernière récupération des données (le 01/09/23) nous avons rencontré des erreurs Argus : "ArgusGenerateRecordStruct: post ARGUS_DATA_DSR len is zero" qui relèvent d'une corruption de paquets transcrits en fichier Argus.
Après un changement d'image de la VM que j'utilisais (par une ubuntu/jammy64 qui correspond à une Ubuntu 22.04) il était possible de constater les mêmes erreurs pour une même version d'Argus/Ra que la 20.04, à savoir la version 3.0.8.2.
Après comparaison, les fichiers de conf "argus.conf" étaient similaires en tout point mais les dépendances utilisées par Argus étaient différentes (diff mysql/mariadb et la version de Perl).
Nous en sommes venus à la conclusion que la version de l'OS influait sur le comportement d'Argus et, de ce fait, le bon déroulement du script. Il est donc nécessaire de prendre en considération ce point avant de tenter l'exécution du script en question -> une VM sera prévue à cet effet avec ma config Vagrant pour la récupération des logs par Hélène.

Avant d'utiliser le script pour process les pcaps il faut s'assurer que les données ne soient pas corrompues (ce qui arrive régulièrement lors de la récupération des pcaps pendant que Suricata est en pleine capture). Ensuite pour merge les données on utilise mergecap avec la commande : **mergecap *.pcap -w « nom_fichier »**. Mergecap a pour avantage d'ordonner correctement les paquets capturés dans différents fichiers.
Si des pcaps sont pollués, on peut les nettoyer avec tshark, par exemple : **tshark -r input.pcap -w output_filtered.pcap -Y "!(ip.src == « IP » or ip.dst == « IP »)"**.
Une fois les données regroupées et filtrées, on peut run le script sur le pcap tout entier.

