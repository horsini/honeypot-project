input_file = "./fast.log"
output_file = "./fast_filtered.log"

"""
    _____________________________________________________________________________________________________

    Script permettant de nettoyer le fichier fast.log généré par Suricata (il faut au préalable unzip les
    multiples fichiers fast.log.gz générés et les concaténer dans un seul fichier fast.log).
    Il est surtout nécessaire lorsqu'on pollue les alertes avec une commande scp executé lorsque Suricata
    tourne toujours.

    Sinon pour les pcap le nettoyage peut se faire de la sorte :
    mergecap *.pcap -w output.pcap -F pcap
    tshark -r input.pcap -w output.pcap -Y "<filter>"
    <filter> peut être par exemple !(ip.src==IP or ip.dst==IP)

    Auteur(s) : [Marius Le Douarin]
    _____________________________________________________________________________________________________
"""

filter = [
    "SURICATA STREAM Packet with invalid timestamp",
    "SURICATA STREAM ESTABLISHED packet out of window",
    "SURICATA STREAM ESTABLISHED invalid ack",
    "SURICATA STREAM Packet with invalid ack"
]

with open(input_file, 'rb') as input_file, open(output_file, 'w') as output_file:
    for line_bytes in input_file:
        try:
            line = line_bytes.decode('utf-8')
            if not any(phrase in line for phrase in filter):
                output_file.write(line)
        except UnicodeDecodeError:
            pass