import os
import csv
from collections import defaultdict
from matplotlib import pyplot as plt

"""
    _________________________________________________________________________________________________________

    Ce script a pour but de générer des statistiques quant aux ports les plus visés par les attaques en fonction
    de différents protocoles précisés via la variable target_string

    Il est nécessaire de le lancer dans le répertoire où les fichiers Netflow sont stockés.

    Auteur(s) : [Marius Le Douarin]
    _________________________________________________________________________________________________________
"""

def delete_rows_by_column_value(input_file, output_file, column_name, target_value, name):
    '''Supprime les lignes d'un fichier CSV dont la valeur d'une colonne est égale à une valeur cible
    
    args:
        input_file: Chemin vers le fichier CSV d'entrée
        output_file: Chemin vers le fichier CSV de sortie
        column_name: Nom de la colonne à vérifier
        target_value: Valeur cible à supprimer
    '''
    with open(input_file, 'r', newline='') as csvfile, open(output_file, 'w', newline='') as outputcsv:
        reader = csv.DictReader(csvfile, delimiter=';')
        fieldnames = reader.fieldnames
        writer = csv.DictWriter(outputcsv, fieldnames=fieldnames, delimiter=';')
        writer.writeheader()
        
        protocol_ports = defaultdict(lambda: defaultdict(int))
        all_ports = defaultdict(int)

        for row in reader:
            if row["Dport"] != '':
                if row[column_name] in target_value:
                    writer.writerow(row)
                    protocol = row["Proto"]
                    protocol_ports[protocol][convert_port_hex_to_int(row["Dport"])] += 1
                all_ports[convert_port_hex_to_int(row["Dport"])] += 1
            
        for protocol, ports in protocol_ports.items():
            plot_pie_chart(ports, protocol, name)
        
        plot_pie_chart(all_ports, "all protocols", name)

def convert_port_hex_to_int(port):
    '''Convertit un port hexadécimal en entier
    
    args:
        port: Port en hexadécimal
    '''
    if isinstance(port, int):
        return port
    try:
        return int(port, 16)
    except ValueError:
        return port

def plot_pie_chart(ports, protocol, name):
    '''Affiche un diagramme circulaire de la répartition des ports

    args:
        ports: Dictionnaire des ports
        protocol: Protocoles
    '''
    labels = []
    sizes = []
    occurences = 1000
    for port in ports:
        if ports[port] > occurences:
            labels.append(port)
            sizes.append(ports[port])
                
    diffPorts = len(labels)
    print("===================\nNombre de ports différents ({}): {}".format(protocol.upper(), diffPorts))
    print("Ports ({}): {}".format(protocol.upper(), labels))
    print("Occurences ({}): {}".format(protocol.upper(), sizes))
        
    explode = [0] * len(labels)
    explode[0] = 0.1
    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
            shadow=False, startangle=90)
    ax1.axis('equal')
    plt.title("Répartition des ports {} (Occurences > {}) - {}".format(protocol.upper(), occurences, name))
   
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.show()

def main():
    script_directory = os.path.dirname(os.path.abspath(__file__))
    repositories = [name for name in os.listdir(script_directory) if os.path.isdir(os.path.join(script_directory, name))]

    for repo_name in repositories:
        column_to_check = "Proto"
        target_string = ["tcp", "udp"]
        repo_path = os.path.join(script_directory, repo_name)
        file_path = ''

        for file_name in os.listdir(repo_path):
            if file_name.endswith(".csv") and "output" in file_name:
                file_path = os.path.join(repo_path, file_name)
               
        if file_path:       
            output_file_name = '_'.join(target_string) + ".csv"
            output_file_path = os.path.join(repo_path, output_file_name)
            delete_rows_by_column_value(file_path, output_file_path, column_to_check, target_string, repo_name)

if __name__ == "__main__":
    main()