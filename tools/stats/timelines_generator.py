import csv
import os
import re
import calendar
from datetime import datetime
import matplotlib.pyplot as plt
from collections import defaultdict
from datetime import date, datetime

"""
    _________________________________________________________________________________________________________

    Ce script a pour but de générer différentes timelines, relatives au nombre de flows capturés à différents
    instants. Finalement nous obtenons des timelines journalières, hebdomadaires, mensuelles, annuelles.

    Il est nécessaire de le lancer dans le répertoire où les fichiers Netflow sont stockés.

    Auteur(s) : [Marius Le Douarin]
    _________________________________________________________________________________________________________
"""

def create_alert_timeline(file_path, output_directory):
    """ Fonction permettant de créer des timelines des alertes sous forme de graphiques
        selon différentes échelles :
            - Journalière
            - Hebdomadaire
            - Mensuelle
            - Annuelle

    args:
        file_path - Chemin vers le fichier de log
        output_directory - Chemin vers le dossier de sortie des graphiques        
    """
    timeline = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(int))))

    with open(file_path, 'r') as file:
        for line in file:
            if line[-1] == '\n':
                line = line[:-1]
            match = re.search(r'^(\d{2}/\d{2}/\d{4})-(\d{2}):(\d{2}):\d{2}', line)
            if match:
                timestamp_str = match.group(1) + "-" + match.group(2) + "-" + match.group(3)
                timestamp = datetime.strptime(timestamp_str, "%m/%d/%Y-%H-%M")
                month = timestamp.strftime("%Y-%m")
                #week = timestamp.strftime("%U") --> Il y a un décalage de semaine pour certains jours
                week = date(timestamp.year, timestamp.month, timestamp.day).isocalendar().week
                day = timestamp.strftime("%m-%d-%Y %a")
                hour = timestamp.hour

                timeline[month][week][day][hour] += 1

    for month, month_data in timeline.items():
        plot_timeline_monthly(month_data, output_directory, f"Month {month}", month)
        for week, week_data in month_data.items():
            plot_timeline_weekly(week_data, output_directory, f"Week {week}")
            for day, day_data in week_data.items():
                plot_timeline_daily(day_data, output_directory, f"Day {day}")
    
    plot_timeline_annually(timeline, output_directory, "Year")

def plot_timeline_daily(timeline, output_directory, title):
    """ Fonction qui crée un graphique de la timeline des alertes pour une journée

    args:
        timeline - Dictionnaire contenant les données de la timeline
        output_directory - Chemin vers le dossier de sortie des graphiques
        title - Titre du graphique
    """
    x_values = list(range(24))
    y_values = [timeline.get(hour, 0) for hour in x_values]

    plt.bar(x_values, y_values)
    plt.xlabel('Hour of the Day')
    plt.title(f'Timeline of alerts - {title}')
    plt.ylabel('# of Alerts')
    plt.tight_layout()
    output_file = f'{output_directory}/timeline_{title.replace(" ", "_").replace("/", "_")}.png'
    plt.savefig(output_file)
    plt.close()

def plot_timeline_weekly(timeline, output_directory, title):
    """ Fonction qui crée un graphique de la timeline des alertes pour une semaine

    args:
        timeline - Dictionnaire contenant les données de la timeline
        output_directory - Chemin vers le dossier de sortie des graphiques
        title - Titre du graphique
    """
    x_values = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    y_values = []
    
    for day in x_values:
        day_key = day[:3]
        day_alerts = [timeline[key].values() for key in timeline if key.endswith(day_key)]
        total_alerts = sum(sum(alerts) for alerts in day_alerts)
        y_values.append(total_alerts)
    
    plt.bar(x_values, y_values)
    plt.xlabel('Day of the Week')
    plt.title(f'Timeline of alerts - {title}')
    plt.ylabel('# of Alerts')
    plt.tight_layout()
    output_file = f'{output_directory}/timeline_{title.replace(" ", "_").replace("/", "_")}.png'
    plt.savefig(output_file)
    plt.close()

def plot_timeline_monthly(timeline, output_directory, title, month):
    """ Fonction qui crée un graphique de la timeline des alertes pour un mois

    args:
        timeline - Dictionnaire contenant les données de la timeline
        output_directory - Chemin vers le dossier de sortie des graphiques
        title - Titre du graphique
        month - Mois de la timeline
    """
    count_days = days_in_month(int(month[:4]), int(month[-2:]))+1
    x_values = list(range(1, count_days))
    y_values = []

    for day in x_values:
        day_key = f"{month}-{day:02d}"
        total_alerts = 0
        for week_data in timeline.values():
            for key in week_data.keys():
                match = re.search(r'^(\d{2})-(\d{2})-(\d{4})', key)
                if match:
                    day_str = match.group(2)
                    month_str = match.group(1)
                    year_str = match.group(3)
                    week_day_key = f"{year_str}-{month_str}-{day_str}"
                    if match and week_day_key == day_key:
                        total_alerts += sum(week_data[key].values())
        y_values.append(total_alerts)
    
    plt.bar(x_values, y_values)
    plt.xlabel('Day of the Month')
    plt.title(f'Timeline of alerts - {title}')
    plt.ylabel('# of Alerts')
    plt.tight_layout()
    output_file = f'{output_directory}/timeline_{title.replace(" ", "_").replace("/", "_")}.png'
    plt.savefig(output_file)
    plt.close()

def plot_timeline_annually(timeline, output_directory, title):
    """ Fonction qui crée un graphique de la timeline des alertes pour une année

    args:
        timeline - Dictionnaire contenant les données de la timeline
        output_directory - Chemin vers le dossier de sortie des graphiques
        title - Titre du graphique
    """
    months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
    current_year = None
    year = None
    y_values = []
    y_value = [0] * len(months)

    for month_data in timeline.keys():
        month = month_data[-2:]
        year = month_data[:4]
        if year != current_year:
            current_year = year
            y_values.append(y_value)
            y_value = [0] * len(months)
        y_value[months.index(month)] = sum(sum(sum(day.values()) for day in week.values()) for week in timeline[month_data].values())

    y_values.append(y_value)        

    if year is not None:
        for y_value in y_values:
            plt.bar(months, y_value)
            plt.xlabel('Month')
            plt.title(f'Timeline of alerts - {title} {year}')
            plt.ylabel('# of Alerts')
            plt.tight_layout()
            output_file = f'{output_directory}/timeline_{title.replace(" ", "_")}_{year}.png'
            plt.savefig(output_file)
            plt.close()

def days_in_month(year, month):
    """ Fonction qui retourne le nombre de jours dans un mois

    args:
        year - Année du mois
        month - Mois
    """
    return calendar.monthrange(year, month)[1]

def read_csv_timestamps(csv_file_path, timestamp_column_name="StartTime"):
    """ Fonction qui extraie les timestamps depuis un fichier CSV d'entrée

    args:
        csv_file_path - Chemin vers le CSV
        timestamp_column_name - Colonne contenant les timestamps, puisque nous avons utilisé l'option stime avec Argus, le nom de la colonne est StartTime

    returns:
        timestamps - La liste de tous les timestamps extraient des fichiers CSV
    """
    timestamps = []
    try:
        with open(csv_file_path, 'r') as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=';')
            for row in csv_reader:
                if timestamp_column_name in row:
                    timestamp_str = row[timestamp_column_name]
                    try:
                        timestamp = datetime.strptime(timestamp_str, "%Y-%m-%d.%H:%M:%S.%f")
                        timestamps.append(timestamp)
                    except ValueError:
                        print("Format de timestamp invalide pour : ", timestamp_str)

        print("Extraction des timestamps réussie !")
        return timestamps
    except FileNotFoundError:
        print("Le fichier CSV : ", csv_file_path, " n'a pas été trouvé ou n'existe pas")
        return []
    except Exception as e:
        print("Erreur lors de la lecture du fichier CSV (corrompu?) : ", str(e))
        return []

def write_timestamps_to_log(timestamps, log_file_path):
    """ Fonction qui écrit les timestamps dans le fichier de log

    args:
        timestamps - La liste des timestamps à écrire
        log_file_path - Chemin vers le fichier de log
    """
    try:
        with open(log_file_path, 'w') as log_file:
            for timestamp in timestamps:
                log_file.write(timestamp.strftime("%m/%d/%Y-%H:%M:%S") + "\n")

        print("Les timestamps ont été correctement écrits dans le fichier de log")
    except Exception as e:
        print("Problème lors de l'écriture dans le fichier de log : ", str(e))

def sort_timestamps(timestamps):
    """ Fonction permettant de réordonner les timestamps dans l'ordre croissant
    Peut-être pas tant nécessaire que ça

    args:
        timestamps - La liste des timestamps à réordonner

    returns:
        sorted_timestamps - Les timestamps réordonnés
    """
    try:
        sorted_timestamps = sorted(timestamps)
        print("Les timestamps sont maintenant dans le bon ordre !")
        return sorted_timestamps
    except Exception as e:
        print("Erreur lors du réordonnement : ", str(e))
        return []
    
def process_directory(directory_path):
    """ Fonction qui effectue le traitement de tous les fichiers CSV d'un dossier

    args:
        directory_path - Chemin vers le dossier contenant les fichiers CSV

    returns:
        timestamps - La liste de tous les timestamps extraits des fichiers CSV
    """
    timestamps = []

    if not os.path.isdir(directory_path):
        print(True)
        return timestamps

    for file_name in os.listdir(directory_path):
        if file_name.endswith(".csv") and "output" in file_name:
            file_path = os.path.join(directory_path, file_name)
            print("Traitement du fichier : ", file_path)
            timestamps.extend(read_csv_timestamps(file_path))

    return timestamps

def main():
    script_directory = os.path.dirname(os.path.abspath(__file__))
    repositories = [name for name in os.listdir(script_directory) if os.path.isdir(os.path.join(script_directory, name))]

    for repo_name in repositories:
        directory_path = os.path.join(script_directory, repo_name) 
        all_timestamps = process_directory(directory_path)
        sorted_timestamps = sort_timestamps(all_timestamps)

        log_file_name = os.path.join(directory_path, "timestamps.log")

        write_timestamps_to_log(sorted_timestamps, log_file_name) 

        timelines_directory = os.path.join(script_directory, "timelines")
        if not os.path.exists(timelines_directory):
            os.makedirs(timelines_directory)

        repo_timelines_directory = os.path.join(timelines_directory, repo_name)
        if not os.path.exists(repo_timelines_directory):
            os.makedirs(repo_timelines_directory)

        create_alert_timeline(log_file_name, os.path.join(script_directory, "timelines", repo_name))

if __name__ == "__main__":
    main()