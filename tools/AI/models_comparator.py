import sys
import time
import pickle
import optuna
import argparse
import importlib
import subprocess
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import cross_validate
from sklearn.semi_supervised import LabelSpreading
from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, confusion_matrix

"""
    _____________________________________________________________________________________________________

    Ce script a pour but de comparer différents modèles de ML en termes de performances sur un coreset.
    Tout d'abord, ce coreset est chargé, à partir duquel X_train, y_train, X_test, y_test sont extraits.
    Chaque modèle a sa propre fonction, qui est appelée dans la fonction main.    
    Les hyperparamètres de ces modèles sont optimisés avec Optuna selon l'accuracy et le f1-score.
    Il est possible d'entraîner un ou plusieurs modèles à la fois.

    Finalement pour chaque modèle on obtient : 
        - Accuray
        - F1 score
        - Precision
        - Recall
        - Matrice de Confusion.

    Les modèles sont :
        - Label Spreading
        - Random Forest
        - Multi-layer Perceptron
        - Gradient Boosting

    Auteurs : [Hélène Orsini, Marius Le Douarin]
    _____________________________________________________________________________________________________
"""

def compute_metrics(y_test, y_pred):
    accuracy = accuracy_score(y_test, y_pred)
    f1 = f1_score(y_test, y_pred, average='macro')
    precision = precision_score(y_test, y_pred, average='macro')
    recall = recall_score(y_test, y_pred, average='macro')
    cm = confusion_matrix(y_test, y_pred)
    return accuracy, f1, precision, recall, cm

def label_spreading(X_train, y_train, X_test, y_test, alpha, kernel, n_neighbors):
    print(alpha, kernel, n_neighbors)
    label_spread = LabelSpreading(alpha=alpha, kernel=kernel, n_neighbors=n_neighbors)
    label_spread.fit(X_train, y_train)
    y_pred = label_spread.predict(X_test)
    return compute_metrics(y_test, y_pred)

# def objective_ls(trial, X_train, y_train, X_test, y_test):
#     alpha = trial.suggest_float('alpha', 0.01, 1.0)
#     kernel = trial.suggest_categorical('kernel', ['knn', 'rbf'])
#     n_neighbors = trial.suggest_int('n_neighbors', 1, 10)
#     accuracy_ls, f1_ls, _, _, _ = label_spreading(X_train, y_train, X_test, y_test, alpha, kernel, n_neighbors)
#     return accuracy_ls

# Cross validation
def objective_ls(trial, X_train, y_train, X_test, y_test):
    alpha = trial.suggest_float('alpha', 0.01, 1.0)
    kernel = trial.suggest_categorical('kernel', ['knn', 'rbf'])
    n_neighbors = trial.suggest_int('n_neighbors', 1, 10)
    accuracy_scores = cross_validate(LabelSpreading(alpha=alpha, kernel=kernel, n_neighbors=n_neighbors, max_iter=3000), X_train, y_train, cv=5, scoring=('accuracy', 'f1_macro'))
    accuracy_ls = accuracy_scores['test_accuracy'].mean()
    f1_ls = accuracy_scores['test_f1_macro'].mean()
    return accuracy_ls, f1_ls

def random_forest(X_train, y_train, X_test, y_test, n_estimators, max_depth):
    rf = RandomForestClassifier(n_estimators=n_estimators, max_depth=max_depth)
    rf.fit(X_train, y_train)
    y_pred = rf.predict(X_test)
    return compute_metrics(y_test, y_pred)

# def objective_rf(trial, X_train, y_train, X_test, y_test):
#     n_estimators = trial.suggest_int('n_estimators', 100, 1000)
#     max_depth = trial.suggest_int('max_depth', 1, 100)
#     accuracy_rf, f1_rf, _, _, _ = random_forest(X_train, y_train, X_test, y_test, n_estimators, max_depth)
#     return accuracy_rf

# Cross validation
def objective_rf(trial, X_train, y_train, X_test, y_test):
    n_estimators = trial.suggest_int('n_estimators', 100, 1000)
    max_depth = trial.suggest_int('max_depth', 1, 100)
    accuracy_scores = cross_validate(RandomForestClassifier(n_estimators=n_estimators, max_depth=max_depth), X_train, y_train, cv=5, scoring=('accuracy', 'f1_macro'))
    accuracy_rf = accuracy_scores['test_accuracy'].mean()
    f1_rf = accuracy_scores['test_f1_macro'].mean()
    return accuracy_rf, f1_rf

def mlp(X_train, y_train, X_test, y_test, hidden_layer_sizes, activation, solver, alpha, learning_rate):
    mlp = MLPClassifier(hidden_layer_sizes=hidden_layer_sizes, activation=activation, solver=solver, alpha=alpha, learning_rate=learning_rate)
    mlp.fit(X_train, y_train)
    y_pred = mlp.predict(X_test)
    return compute_metrics(y_test, y_pred)

# def objective_mlp(trial, X_train, y_train, X_test, y_test):
#     hidden_layer_sizes = trial.suggest_int('hidden_layer_sizes', 1, 100)
#     activation = trial.suggest_categorical('activation', ['identity', 'logistic', 'tanh', 'relu'])
#     solver = trial.suggest_categorical('solver', ['lbfgs', 'sgd', 'adam'])
#     alpha = trial.suggest_float('alpha', 0.0001, 1.0)
#     learning_rate = trial.suggest_categorical('learning_rate', ['constant', 'invscaling', 'adaptive'])
#     accuracy_mlp, f1_mlp, _, _, _ = mlp(X_train, y_train, X_test, y_test, hidden_layer_sizes, activation, solver, alpha, learning_rate)
#     return accuracy_mlp

# Cross validation
def objective_mlp(trial, X_train, y_train, X_test, y_test):
    hidden_layer_sizes = trial.suggest_int('hidden_layer_sizes', 1, 100)
    activation = trial.suggest_categorical('activation', ['identity', 'logistic', 'tanh', 'relu'])
    solver = trial.suggest_categorical('solver', ['lbfgs', 'sgd', 'adam'])
    alpha = trial.suggest_float('alpha', 0.0001, 1.0)
    learning_rate = trial.suggest_categorical('learning_rate', ['constant', 'invscaling', 'adaptive'])
    accuracy_scores = cross_validate(MLPClassifier(hidden_layer_sizes=hidden_layer_sizes, activation=activation, solver=solver, alpha=alpha, learning_rate=learning_rate), X_train, y_train, cv=5, scoring=('accuracy', 'f1_macro'))
    accuracy_mlp = accuracy_scores['test_accuracy'].mean()
    f1_mlp = accuracy_scores['test_f1_macro'].mean()
    return accuracy_mlp, f1_mlp

def gradient_boosting(X_train, y_train, X_test, y_test, n_estimators, max_depth):
    gb = GradientBoostingClassifier(n_estimators=n_estimators, max_depth=max_depth)
    gb.fit(X_train, y_train)
    y_pred = gb.predict(X_test)
    return compute_metrics(y_test, y_pred)

# def objective_gb(trial, X_train, y_train, X_test, y_test):
#     n_estimators = trial.suggest_int('n_estimators', 100, 1000)
#     max_depth = trial.suggest_int('max_depth', 1, 100)
#     accuracy_gb, f1_gb, _, _, _ = gradient_boosting(X_train, y_train, X_test, y_test, n_estimators, max_depth)
#     return accuracy_gb

# Cross validation
def objective_gb(trial, X_train, y_train, X_test, y_test):
    n_estimators = trial.suggest_int('n_estimators', 100, 1000)
    max_depth = trial.suggest_int('max_depth', 1, 100)
    accuracy_scores = cross_validate(GradientBoostingClassifier(n_estimators=n_estimators, max_depth=max_depth), X_train, y_train, cv=5, scoring=('accuracy', 'f1_macro'))
    accuracy_gb = accuracy_scores['test_accuracy'].mean()
    f1_gb = accuracy_scores['test_f1_macro'].mean()
    return accuracy_gb, f1_gb

def display_metrics(accuracy, f1, precision, recall, cm):
    print("Accuracy : ", accuracy)
    print("F1-score : ", f1)
    print("Precision : ", precision)
    print("Recall : ", recall)
    
    # Create a heatmap of the confusion matrix
    plt.figure(figsize=(8, 6))
    sns.heatmap(cm, annot=True, fmt="d", cmap="Blues", cbar=False)
    plt.xlabel("Predicted")
    plt.ylabel("Actual")
    plt.title("Confusion Matrix")
    plt.show()

def check_dependency(dependency):
    try:
        importlib.import_module(dependency)
    except ImportError:
        return False
    return True

def install_dependency(dependency):
    subprocess.check_call([sys.executable, "-m", "pip", "install", dependency])

def main():
    description = """\033[31mEntraînement de modèles de ML sur un coreset. \033[0m
    Modèles disponibles :
    - Label Spreading 👉 label_spreading
    - Random Forest 👉 random_forest
    - Multi-Layer Perceptron 👉 mlp
    - Gradient Boosting 👉 gradient_boosting"""

    parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("model", nargs="*", help="Choisissez le ou les modèles à entraîner, délimité(s) par des espaces")
    args = parser.parse_args()

    if not args.model:
        parser.print_help()
        exit()

    required_dependencies = ["optuna", "scikit-learn", "matplotlib"]

    missing_dependencies = [dep for dep in required_dependencies if not check_dependency(dep)]

    if missing_dependencies:
        print("Le script nécessite cette/ces dépendance(s) manquante(s) :")
        for dep in missing_dependencies:
            print(f" - '{dep}'")

        choice = input("Faut-il la/les installer maintenant ? (O/n): ").strip().lower()
        
        if choice == 'o':
            for dep in missing_dependencies:
                install_dependency(dep)
        else:
            print("======================================\nLe script peut s'exécuter anormalement\nLa séquence se poursuit mais risque d'être interrompue", end="")

            for _ in range(3):
                time.sleep(1)
                print(".", end="", flush=True)

    print("\n\n=== Début de la comparaison des modèles ===")

    # ! CORESET
    with open('coreset/10nngraphdict_coreset__restrict_5_nearestn_0.03_trueyspread.pkl', 'rb') as f:
        ty  = pickle.load(f)
    with open('coreset/10nngraphdict_coreset__restrict_5_nearestn_0.03_Xspread.pkl', 'rb') as f:
        x  = pickle.load(f)
    with open('coreset/10nngraphdict_coreset__restrict_5_nearestn_0.03_yspread.pkl', 'rb') as f:
        y  = pickle.load(f)

    train_point = y != -1
    y_train = y[train_point]
    X_train = x[train_point]
    y_test = ty
    X_test = x
    
    for model_choice in args.model:
        if model_choice == "label_spreading":
            print("\n=== Label Spreading ===")
            study_ls = optuna.create_study(directions=['maximize', 'maximize'])
            study_ls.optimize(lambda trial: objective_ls(trial, X_train, y_train, X_test, y_test), n_trials=10)
            best_params_ls = study_ls.best_trials[0].params
            accuracy_ls, f1_ls, precision_ls, recall_ls, cm_ls = label_spreading(X_train, y_train, X_test, y_test, **best_params_ls)
            display_metrics(accuracy_ls, f1_ls, precision_ls, recall_ls, cm_ls)
        elif model_choice == "random_forest":
            print("\n=== Random Forest ===")
            study_rf = optuna.create_study(directions=['maximize', 'maximize'])
            study_rf.optimize(lambda trial: objective_rf(trial, X_train, y_train, X_test, y_test), n_trials=10)
            best_params_rf = study_rf.best_trials[0].params
            accuracy_rf, f1_rf, precision_rf, recall_rf, cm_rf = random_forest(X_train, y_train, X_test, y_test, **best_params_rf)
            display_metrics(accuracy_rf, f1_rf, precision_rf, recall_rf, cm_rf)
        elif model_choice == "mlp":
            print("\n=== Multi-layer Perceptron ===")
            study_mlp = optuna.create_study(directions=['maximize', 'maximize'])
            study_mlp.optimize(lambda trial: objective_mlp(trial, X_train, y_train, X_test, y_test), n_trials=10)
            best_params_mlp = study_mlp.best_trials[0].params
            accuracy_mlp, f1_mlp, precision_mlp, recall_mlp, cm_mlp = mlp(X_train, y_train, X_test, y_test, **best_params_mlp)
            display_metrics(accuracy_mlp, f1_mlp, precision_mlp, recall_mlp, cm_mlp)
        elif model_choice == "gradient_boosting":
            print("\n=== Gradient Boosting ===")
            study_gb = optuna.create_study(directions=['maximize', 'maximize'])
            study_gb.optimize(lambda trial: objective_gb(trial, X_train, y_train, X_test, y_test), n_trials=10)
            best_params_gb = study_gb.best_trials[0].params
            accuracy_gb, f1_gb, precision_gb, recall_gb, cm_gb = gradient_boosting(X_train, y_train, X_test, y_test, **best_params_gb)
            display_metrics(accuracy_gb, f1_gb, precision_gb, recall_gb, cm_gb)
        else:
            print(f"Le modèle : {model_choice} est invalide")

    print("\n=== Fin de la comparaison des modèles ===")

if __name__ == "__main__":
    main()