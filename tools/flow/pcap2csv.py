#!/usr/bin/python
# -*- coding: utf-8 -*-
import argparse
import os
import subprocess
import csv

"""
    _____________________________________________________________________________________________________

    Ce script a pour but de convertir une liste de fichiers PCAP, contenus dans un répertoire donné, 
    en fichiers CSV. Ces fichiers CSV sont ensuite concaténés en un seul fichier CSV : concatenated.csv.
    Si des fichiers CSV existent déjà, ils ne sont pas écrasés et le script passe au fichier suivant sans
    appliquer le processus de conversion.

    Le fichier .rarc est nécessaire pour la conversion puisqu'il permet de définir le format des timestamps.
    Par défaut, le formatat est "%H:%M:%S.%f" (Heure:Minute:Seconde.Microseconde) or le format qui nous
    intéresse réellement est "%Y-%m-%d %H:%M:%S.%f" (Année-Mois-Jour Heure:Minute:Seconde.Microseconde).

    La conversion s'effectue en deux étapes : PCAP -> Argus -> CSV. Le fichier Argus est ensuite supprimé.
    La conversion Argus -> CSV est effectuée grâce à « ra » (pour « Read Argus ») et ne fait qu'extraire
    des champs des fichiers Argus. La liste des champs est ici donnée dans la variable argus_options.

    Auteur(s) : [Marius Le Douarin]
    _____________________________________________________________________________________________________
"""

# Répertoire où trouver les logs
script_directory = os.path.dirname(os.path.abspath(__file__))
root_directory = ""
subfolder = "csv_files"
rarc_file = os.path.join(script_directory, ".rarc")

def concatenate_csv(directory, output_file):

    # Création du fichier de sortie final
    with open(output_file, "w", newline="") as output_csv:

        # On définit le même délimiteur que les fichiers CSV
        csv_writer = csv.writer(output_csv, delimiter=";")

        # Booléen pour vérifier si l'entête du fichier CSV a déjà été écrite
        csv_header_written = False

        # On itère sur tous les fichiers du répertoire
        for file_name in os.listdir(directory):

            # On ne prend pas en compte le fichier créé par le script eve2csv.py
            if file_name.endswith(".csv") and file_name != "concatenated.csv":
                file_path = os.path.join(directory, file_name)
                with open(file_path, "r") as input_csv:
                    csv_reader = csv.reader(input_csv, delimiter=";")

                    # On itère sur toutes les lignes du fichier CSV
                    for i, row in enumerate(csv_reader):

                        # On écrit l'entête du fichier CSV une seule fois
                        # Eventuellement on pourrait simplement vérifier si le fichier est vide
                        if i==0 and not csv_header_written:
                            csv_header_written = True
                            csv_writer.writerow(row)

                        # On ne réécrit pas l'entête du fichier CSV en passant la première ligne
                        elif i==0 and csv_header_written:
                            continue

                        # On écrit toutes les autres lignes
                        else:
                            csv_writer.writerow(row)

def main():
    description = """\033[31mConvertisseur PCAP vers Netflow. \033[0m
    Le script nécessite d'installer argus-client et de préciser le répertoire contenants les fichiers PCAP à convertir."""

    parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("path", nargs=1, help="Précisez le path du répertoire")
    args = parser.parse_args()

    if not args.path:
        parser.print_help()
        exit()

    root_directory = args.path[0]

    if not os.path.exists(root_directory):
        print(f"Le répertoire '{root_directory}' n'est pas un répertoire ou n'existe pas.")
        exit()

    # Vérifier et créer le dossier subfolder s'il n'existe pas
    csv_folder = os.path.join(root_directory, subfolder)
    if not os.path.exists(csv_folder):
        os.makedirs(csv_folder)

    # On cherche les fichiers PCAP
    pcap_files = [file for file in os.listdir(root_directory) if os.path.isfile(os.path.join(root_directory, file)) and "pcap" in file]

    # On itère sur tous les fichiers PCAP de la liste pcap_files
    for index, pcap_file in enumerate(pcap_files, start=1):

        # Chemin complet du fichier PCAP
        pcap_file_path = os.path.join(root_directory, pcap_file)

        # Chemin complet du fichier Argus
        argus_file_path = os.path.join(root_directory, pcap_file.replace(".pcap", ".argus"))

        # Chemin complet du fichier CSV
        csv_file_path = os.path.join(root_directory, subfolder, pcap_file.replace(".pcap", ".csv"))

        # Check si le fichier CSV existe déjà ou si le PCAP est vide
        if os.path.exists(csv_file_path) or os.path.getsize(pcap_file_path) == 0:
            continue

        # Conversion du fichier PCAP en ficher Argus
        subprocess.run(["/usr/sbin/argus", "-r", pcap_file_path, "-w", argus_file_path])

    # On cherche les fichiers Argus
    argus_files = [file for file in os.listdir(root_directory) if os.path.isfile(os.path.join(root_directory, file)) and "argus" in file]

    # On itère sur tous les fichiers Argus de la liste argus_files
    for index, argus_file in enumerate(argus_files, start=1):

        # Chemin complet du fichier Argus
        argus_file_path = os.path.join(root_directory, argus_file)

        # Check si le fichier Argus n'existe pas
        if not os.path.exists(argus_file_path):
            continue

        # Chemin complet du fichier CSV
        csv_file_path = os.path.join(root_directory, subfolder, argus_file.replace(".argus", ".csv"))

        '''
            Options à extraire du fichier Argus, à savoir :
            - stime : Le timestamp du premier paquet d'une connexion
            - dur : La durée de la connexion
            - proto : Le protocole utilisé
            - saddr : L'adresse IP source
            - sport : Le port source
            - dir : La direction de la connexion
            - daddr : L'adresse IP de destination
            - dport : Le port de destination
            - state : L'état de la connexion
            - stos : Le type de service source
            - dtos : Le type de service de destination
            - pkts : Le nombre de paquets
            - bytes : Le nombre d'octets
            - sbytes : Le nombre d'octets source
        '''
        argus_options = "stime,dur,proto,saddr,sport,dir,daddr,dport,state,stos,dtos,pkts,bytes,sbytes"

        # Conversion du fichier Argus en CSV
        subprocess.run(["/usr/bin/ra", "-r", argus_file_path, "-c", ";", "-s", argus_options, "-F", rarc_file], stdout=open(csv_file_path, "w"))

        # Supression du fichier Argus
        os.remove(argus_file_path)

    # Chemin du dossier où se trouvent les fichiers CSV
    destination_dir = os.path.join(root_directory, subfolder)

    # On concatène tous les fichiers CSV en un seul : concatenated.csv
    concatenate_csv(destination_dir, os.path.join(destination_dir, "concatenated.csv"))

if __name__ == "__main__":
    main()
