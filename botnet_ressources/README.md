Ressources
===

## Objective
Collect different ressources, sites, images, papers to deisgn, develop and justify our experiments


## List

- cycle_de_vie_d_un_botnet_et_de_ses_bots.png : image showing the life cycle of a botnet from https://theses.hal.science/tel-01231974/preview/these_archivage_3159144o.pdf

## Useful websites
- https://theses.hal.science/tel-01231974/preview/these_archivage_3159144o.pdf : thesis on Lutte contre les botnets : analyse et stratégie
- https://www.clickguard.com/blog/recent-botnet-attacks-2022/
76 Chapitre 2. Collecte d’informations
- https://www.virustotal.com
- https://malwr.com/
- https://www.hybrid-analysis.com/
- http://anubis.iseclab.org
- https://www.botnets.fr/wiki/Accdfisa


