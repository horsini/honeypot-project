Botnets benchmark
===

https://www.malwarebytes.com/blog?s=botnet


| Botnet | When? | SSH | Vulnerability path | Objective | Links|
|:---------- | :----------------------------------------------------- | :------------ | ----------------------------------------------------------- | :-------------------------------------- |------------------------|
|   Xor DDoS   |   old 8y    |     yes                |  bruteforce ssh credetnials, then install malware and use XOR encryption to exchnage with C&C server         | use to run DDoS attack |https://www.zdnet.fr/actualites/microsoft-s-inquiete-de-l-expansion-du-botnet-xor-ddos-39942465.html https://www.blackalps.ch/ba-17/files/talks/BlackAlps17-Tafani.pdf|
| Mirai |2016|yes||||
|Mozi||||||
|Sysrv||yes||||
|RapperBot (mirai-based)|2022|yes|||https://duo.com/decipher/linux-iot-botnet-targets-weak-ssh-server-credentials|
|FritzFrog||yes|FritzFrog spreads by scanning the Internet for SSH servers, and when it finds one, it attempts to log in using a list of credentials. When successful, the botnet software installs proprietary malware that makes it a drone in a sprawling, headless P2P network. Each server constantly listens for connections on port 1234 while simultaneously scanning thousands of IP addresses over ports 22 and 2222.||https://arstechnica.com/information-technology/2022/02/after-lying-low-ssh-botnet-mushrooms-and-is-harder-than-ever-to-take-down/ https://www.lebigdata.fr/malware-fritzfrog-revient|
|Dubbed PyCryptoMiner|2017|yes|The botnet spreads by attempting to guess the SSH login credentials of target Linux machines. If the credentials are successfully discovered, the attacking bot deploys a simple base64-encoded spearhead Python script designed to connect to the C&C server to download and execute additional Python code. The second-stage code is the main bot controller, which registers a cron job on the infected machine to create persistency. | crypto-mining or scanning|https://www.securityweek.com/crypto-miner-botnet-spreads-over-ssh/|https://www.securityweek.com/crypto-miner-botnet-spreads-over-ssh/|
