import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from pyvirtualdisplay import Display
from decouple import config
import random
import os
import sys

def random_wait(base_wait):
    additional_wait = random.randint(0, 5)
    total_wait = base_wait + additional_wait
    time.sleep(total_wait)

def daily_task():
    # Config PyVirtualDisplay
    display = Display(visible=0, size=(1920, 1080))
    display.start()
    # Config Selenium
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--window-size=1920,1080")
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--proxy-server='direct://'")
    chrome_options.add_argument("--proxy-bypass-list=*")
    chrome_options.add_argument("--start-maximized")
    #chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--ignore-certificate-errors')
    chromedriver_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "chromedriver")
    service = Service(chromedriver_path)
    driver = webdriver.Chrome(service=service, options=chrome_options)
    driver.maximize_window()

    driver.get("https://www.google.com/")
    cookie_consent = driver.find_element(By.ID, "L2AGLb")
    cookie_consent.click()
    search_bar = driver.find_element(By.NAME, "q")
    search_bar.send_keys("gmail")
    search_bar.send_keys(Keys.ENTER)
    random_wait(2)

    email_address = os.environ.get('MAIL_ADDR')
    password = os.environ.get('MAIL_PWD')

    # Chargement de la page
    driver.get("https://www.google.com/intl/fr/gmail/about/")
    # On récupère les boutons de la page
    buttons_header = driver.find_element(By.CLASS_NAME, "header__aside__buttons")
    # On clique sur celui permettant de se log
    buttons_header.find_element(By.CSS_SELECTOR, "a.button--medium").click()
    random_wait(2)
    # On récupère l'input pour entrer l'adresse mail
    email_input = driver.find_element(By.CSS_SELECTOR, 'input[type="email"]')
    #email_input.send_keys(f"{config('mail_address')}")
    email_input.send_keys(email_address)
    # Bouton suivant
    driver.find_element(By.CSS_SELECTOR, '.VfPpkd-LgbsSe.VfPpkd-LgbsSe-OWXEXe-k8QpJ.VfPpkd-LgbsSe-OWXEXe-dgl2Hf.nCP5yc.AjY5Oe.DuMIQc.LQeN7.qIypjc.TrZEUc.lw1w4b').click()
    #driver.find_element(By.CLASS_NAME, "VfPpkd-Jh9lGc").click()
    random_wait(50)
    # On récupère l'input pour entrer le mot de passe
    password_input = driver.find_element(By.CSS_SELECTOR, 'input[type="password"]')
    #password_input.send_keys(f"{config('mail_passwd')}")
    password_input.send_keys(password)
    driver.find_element(By.CSS_SELECTOR, '.VfPpkd-LgbsSe.VfPpkd-LgbsSe-OWXEXe-k8QpJ.VfPpkd-LgbsSe-OWXEXe-dgl2Hf.nCP5yc.AjY5Oe.DuMIQc.LQeN7.qIypjc.TrZEUc.lw1w4b').click()

    random_wait(7)
   
    # On récupère les mails
    inbox_emails = driver.find_elements(By.XPATH, "//*[contains(@class, 'zA') and contains(@class, 'zE')]")
    print("Nouveaux mails : " + str(len(inbox_emails)))
    for email in inbox_emails:
        # email.click() clique au niveau des pièces jointes et clique donc sur la prévisualisation de l'une d'entre elles quand il y en a beaucoup
        click_box = email.find_element(By.XPATH, ".//td[@class='xY a4W']")
        click_box.click()
        #email.click()
        random_wait(10)
        try:
            attachment_div = driver.find_element(By.XPATH, "//div[@class='aQH']")
            attachment = attachment_div.find_elements(By.XPATH, ".//span[contains(@class, 'aZo')]")
            print("Nombre de fichiers à DL : " + str(len(attachment)))
            if len(attachment) > 0:
                for attachment_element in attachment:
                    attachment_link = attachment_element.find_element(By.XPATH, ".//a")
                    attachment_url = attachment_link.get_attribute("href")
                    driver.get(attachment_url)
        except:
            pass

        # Bouton répondre
        driver.find_element(By.CLASS_NAME, "ams.bkH").click()
        random_wait(1)
        # Lecture des phrases depuis le fichier
        with open('pony.txt', 'r', encoding='utf-8') as file:
            quotes = file.readlines()
        # Choix aléatoire d'une phrase parmi la liste
        answer = "Super merci c'est noté,\n\nD'ailleurs je voulais partager avec toi un fait intéressant sur les poneys, savais-tu que :\n" + random.choice(quotes).strip() + "\n\nBonne journée et à bientôt!"
        # On récupère le champ de texte et on y écrit un message
        driver.find_element(By.CLASS_NAME, "Am.aO9.Al.editable.LW-avf.tS-tW").send_keys(answer)
        # On envoie le mail en cliquant sur le bouton Envoyer
        driver.find_element(By.CLASS_NAME, "T-I.J-J5-Ji.aoO.v7.T-I-atl.L3").click()
        random_wait(1)
        # Revenir à la page précédente
        driver.execute_script("window.history.go(-1)")
        random_wait(2)
    # On ferme la fenêtre
    display.stop()

daily_task()
