import random
import subprocess
import sys
import json
import time
import os

# Chargement des profiles depuis le fichier de configuration JSON
def load_profiles():
    with open('/usr/local/bin/scripts/profiles.json') as file:
        data = json.load(file)
    return data['profiles']

# Execution des scripts en fonction du profil choisi
def execute_scripts(profile_name):
    print("Début de la routine quotidienne")
   
    # On charge les profils pour trouver celui correspondant au nom passé en argument
    profiles = load_profiles()
    selected_profile_data = next((p for p in profiles if p['name'].lower() == profile_name.lower()), None)

    # On exécute les scripts du profil
    scripts_to_execute = selected_profile_data.get('scripts', [])
    print(f"Scripts à exécuter : {scripts_to_execute}")
    for script in scripts_to_execute:

        script_path = os.path.join("/usr/local/bin/", script)

        # Lancement des différents scripts
        print(f"Prochain à exécuter : {script}")
        if "mailing" in script.lower():
            arguments = [sys.argv[2], sys.argv[3]]
            subprocess.call([sys.executable, script_path] + arguments)
        else:
            subprocess.call([sys.executable, script_path])

        # Si on a plusieurs scripts, on attend un peu entre chaque
        if len(scripts_to_execute) > 2:
            time.sleep(random.randint(10, 25))
    print("Fin de la routine quotidienne")

# Création d'un ordre aléatoire d'exécution
def shuffle_scripts(scripts):
    random.shuffle(scripts)
    return scripts

# On récupère le nom du profil à exécuter depuis les arguments
selected_profile = sys.argv[1]
execute_scripts(selected_profile)

# # Tous les scripts
# files = os.listdir()
# # Sauf le script courant
# current_script = os.path.basename(__file__)
# files.remove(current_script)
# files.remove("pony.txt")
# # Crée l'ordre aléatoire
# random.shuffle(files)
# # On joue les scripts un à un dans l'order aléatoire
# for script in scripts_to_execute:
#     print(f"Script en cours : {script}")
#     subprocess.call([sys.executable, script])