# suricata-deployment

## 📂 Important files :

* [Vagrantfile](/honeypot-projet/proto-marius/Vagrantfile) The Vagrantfile in this project sets up the virtual machines required to run the honeypots. It specifies the base box, networking settings, and provisions the machines using Ansible. You might need to change the cluster configuration to define virtual machines, matching with your needs.

* [Suricata playbook](/honeypot-projet/proto-marius/suricata-deployment.yml) 
This playbook automates the installation and configuration of Suricata.
Suricata will monitor network traffic and detect potential threats. It will perform the following tasks:

    - Installs the required dependencies, including software-properties-common.
    - Adds the Suricata repository ppa:oisf/suricata-stable.
    - Installs the Suricata package.
    - Modifies the suricata.yaml configuration file to customize various settings, including HOME_NET, replaces specific lines in the suricata.yaml file related to af-packet, pcap, and netflow configurations, modifies the suricata.yaml file to authorize pcap logging and adjust memory.
    - Executes suricata-update command to generate /var/lib/suricata/rules.
    - Starts the Suricata service using systemd with autoreboot

* [Honeypot playbook](/honeypot-projet/proto-marius/honeypot-deployment.yml) This playbook change the SSH port for a specific virtual machine, I will generalize it.

* [Suricata service playbook](/honeypot-projet/proto-marius/suricata-reboot.yml) To fully generate pcap logs, we have no choice but not end the current flow running on the Suricata machine. This playbook reboots the Suricata service with systemd (not working as intented for now).

* [Synced folders](/honeypot-projet/proto-marius/logs/) This repository contains subdirectories that correspond to the synchronized directories of all Suricata machines. Each subdirectory represents a specific machine and contains the synchronized files and directories for that machine :
```cluster.each do |hostname, _|
  if hostname.start_with?('suricata')
    log_folder = "logs/#{hostname}"
    `mkdir -p #{log_folder}` unless Dir.exist?(log_folder)
  end

    [...]
    subcfg.vm.synced_folder "logs/#{hostname}/", "/var/log/suricata/"
``` 

* [Preprocessing script](/honeypot-projet/proto-marius/suricata_logs_preprocessing.py) This script will be executed as a Cronjob right after restarting the Suricata service (I still have to configure it in the [Suricata playbook](/honeypot-projet/proto-marius/suricata-deployment.yml)). It will retrieve all pcap logs, then I am still trying to figure out how to convert it into Netflow CSV. The commented part in the script uses Argus so we have :

    - PCAP to Argus file (done remotely)
    - Argus file to CSV with selected fields ➜ **Duration** - **Total transaction packet count** - **Total transaction bytes** - **SRC -> DST transaction bytes** - **Transaction protocol** - **Source TOS byte value** - **Destination TOS byte value** - **Transaction state** - **Destination port** - **Source port**

Subdirectories have a [concatenated.csv](https://gitlab.inria.fr/horsini/honeypot-project/-/blob/main/proto-marius/suricata-deployment/logs_retrieved/suricata1/concatenated.csv) file which is the concatenation of all CSV files obtained from Argus fields extraction.

Another option is based on the eve.json logs located in each Suricata subdirectory. The [eve2csv.py](/honeypot-projet/proto-marius/eve2csv.py) script retrieves these informations, but a lot are missing, here's an example ➜ [suricata.csv](/honeypot-projet/proto-marius/logs_retrieved/suricata1/suricata.csv)

* [logs_retrieved](honeypot-projet/proto-marius/logs_retrieved/) is the folder with all pre-processed logs, both suricata.csv and individual PCAP files converted.

## 🔧 Deploy it
After cloning the project :
* 👉 **vagrant up** to launch the Vagrantfile.
* 👉 **vagrant provision** to rerun all Ansible playbooks.
* 👉 **vagrat destroy name** to stop the { name } virtual machine or **vagrant destroy** to destroy them all
* 👉 **vagrant gloabal-status** to see which virtual machines are running.
