import os
import subprocess

# Répertoire où trouver les logs
root_directory = "/var/log/suricata"

# On cherche les fichiers pcap
pcap_files = [file for file in os.listdir(root_directory) if os.path.isfile(os.path.join(root_directory, file)) and "pcap" in file and "stats" not in file]
for index, pcap_file in enumerate(pcap_files, start=1):
    
    # Chemin complet du fichier PCAP
    pcap_file_path = os.path.join(root_directory, pcap_file)

    # On vérifie que le fichier n'est pas en train d'être écrit
    if os.path.getsize(pcap_file_path) > 0:
    
        # Répertoire de destination pour le fichier Argus
        argus_file_path = os.path.join(root_directory, pcap_file.replace(".pcap", ".argus"))

        # Check si le fichier Argus n'existe pas déjà
        if not os.path.exists(argus_file_path):

            # Conversion du fichier PCAP en ficher Argus
            subprocess.run(["argus", "-r", pcap_file_path, "-w", argus_file_path])