import sys
import time
import os
import smtplib
import email.utils
from email.mime.text import MIMEText
import glob

# Fichier de log Suricata
suricata_log = "./logs_retrieved"

# Mails expéditeur et destinataire
email_from = "suricata.poneypot.alerts@gmail.com"
email_to = "" # Liste à déterminer

# Fonction de parsage des alertes
def parse_logline(s):
    row = [ ]
    sline = s.split("[**]")
    row = [sline[1].strip(), sline[2].strip()]
    return row

# Fonction de récupération de la dernière alerte traitée
def tail(file_path):
    try:
        with open(file_path, 'r') as file:
            # On utilise seek pour se positionner à la fin du fichier
            file.seek(0, 2)
            # Retourne la position actuelle du curseur (à la fin du fichier)
            return file.tell()
    except IOError:
        # On commence au début du fichier
        return 0

# Fonction de traitement des nouvelles alertes
def process_new_alerts(suricata_log):
    subdirectories = glob.glob(os.path.join(suricata_log, "suricata*"))

    #Dictonniaire pour stocker la dernière position traitée pour chaque fichier pour éviter les doublons d'alertes traitées et éviter le spam
    last_processed_positions = {}  
    for subdirectory in subdirectories:
        file_path = os.path.join(subdirectory, "fast.log")
        if not os.path.isfile(os.path.join(file_path)):
            print("No fast.log found in {0}".format(subdirectory))
            sys.exit(1)
        # Dernière position traitée pour le fichier fasT.log du sous-répertoire courant
        last_processed_position = tail(file_path)
        last_processed_positions[subdirectory] = last_processed_position
    
    # Boucle infinie pour traiter les nouvelles alertes
    while True:
        for subdirectory in subdirectories:
            file_path = os.path.join(subdirectory, "fast.log")
            # On récupère la dernière position traitée pour le fichier fast.log du sous-répertoire courant
            last_processed_position = last_processed_positions[subdirectory]
            file_size = os.path.getsize(file_path)
            # On check si le fichier a été modifié depuis la dernière position traitée
            if file_size > last_processed_position:
                with open(file_path, 'r') as file:
                    file.seek(last_processed_position)
                    lines = file.readlines()
                    last_processed_position = file.tell()
                    # On update la dernière position traitée
                    last_processed_positions[subdirectory] = last_processed_position

                # On itère sur les alertes à traiter
                for line in lines:
                    try:
                        logline = parse_logline(line)
                        mail_notification("🚨 Suricata alert on ➤ {0}\n{1}\n{2}".format(os.path.basename(subdirectory), logline[0], logline[1]))
                        print("Sent!")
                    except Exception as e:
                        print("Error:", str(e))
            else:
                time.sleep(1)

def mail_notification(logmsg):
    msg = MIMEText(logmsg)
    msg['To'] = email.utils.formataddr(("Suricata Mailing List", email_to))
    msg['From'] = email.utils.formataddr(("🛡️Suricata Admin🛡️", email_from))
    msg['Subject'] = "[SURICATA] Alerte de sécurité"
    server = smtplib.SMTP('localhost')
    try:
        server.sendmail(email_to, [email_to], msg.as_string())
    except Exception as e:
        print("Error: {0}".format(e))
    finally:
        server.quit()

# Démarrage du monitoring des alertes Suricata
process_new_alerts(suricata_log)