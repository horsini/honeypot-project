from collections import defaultdict
import csv

from matplotlib import pyplot as plt

def delete_rows_by_column_value(input_file, output_file, column_name, target_value):
    '''Supprime les lignes d'un fichier CSV dont la valeur d'une colonne est égale à une valeur cible
    
    args:
        input_file: Chemin vers le fichier CSV d'entrée
        output_file: Chemin vers le fichier CSV de sortie
        column_name: Nom de la colonne à vérifier
        target_value: Valeur cible à supprimer
    '''
    with open(input_file, 'r', newline='') as csvfile, open(output_file, 'w', newline='') as outputcsv:
        reader = csv.DictReader(csvfile, delimiter=';')
        fieldnames = reader.fieldnames
        writer = csv.DictWriter(outputcsv, fieldnames=fieldnames, delimiter=';')
        writer.writeheader()
        count = 0
        
        protocol_ports = defaultdict(lambda: defaultdict(int))
        all_ports = defaultdict(int)

        for row in reader:
            if row[column_name] in target_value:
                writer.writerow(row)
                protocol = row["Proto"]
                protocol_ports[protocol][row["Dport"]] += 1
            if row[column_name] in target_value or not target_value:
                if (row["Dport"] == "sip" and row["Proto"] == "udp"):
                    count += 1
                all_ports[row["Dport"]] += 1
        
        print("SIP : " + str(count))
        
        for protocol, ports in protocol_ports.items():
            plot_pie_chart(ports, protocol)
        
        plot_pie_chart(all_ports, "all protocols")

def plot_pie_chart(ports, protocol):
    labels = []
    sizes = []
    occurences = 60
    for port in ports:
        if ports[port] > occurences:
            labels.append(port)
            sizes.append(ports[port])
                
    diffPorts = len(labels)
    print("Nombre de ports différents ({}): {}".format(protocol.upper(), diffPorts))
    print("Ports ({}): {}".format(protocol.upper(), labels))
    print("Occurences ({}): {}".format(protocol.upper(), sizes))
        
    # Pie chart
    explode = [0] * len(labels)
    explode[0] = 0.1
    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
            shadow=False, startangle=90)
    ax1.axis('equal')
    plt.title("Répartition des ports {} (Occurences > {})".format(protocol.upper(), occurences))
   
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.show()

if __name__ == "__main__":
    input_file_path = ""
    output_file_path = ""
    column_to_check = "Proto"
    target_string = ["tcp", "udp"]

    delete_rows_by_column_value(input_file_path, output_file_path, column_to_check, target_string)
