import csv
from datetime import datetime
import os

def extract_time_column_to_log(csv_file_path, log_file_path):
    try:
        with open(csv_file_path, 'r', newline='') as csv_file, open(log_file_path, 'w') as log_file:
            csv_reader = csv.DictReader(csv_file)
            if "time" not in csv_reader.fieldnames:
                print("Error: 'time' column not found in the CSV file.")
                return
            
            for row in csv_reader:
                time_value = row["time"]
                dt_obj = datetime.fromtimestamp(float(time_value))
                print(dt_obj)
                log_file.write(str(dt_obj) + "\n")
                
        print("Extraction complete. Time column has been written to the log file.")
    except FileNotFoundError:
        print("Error: CSV file not found. Path:", os.path.abspath(csv_file_path))
    except Exception as e:
        print("An error occurred:", str(e))

csv_file_path = "./csv_files/csv_files.csv"
log_file_path = "./csv_files/timestamps.log"
extract_time_column_to_log(csv_file_path, log_file_path)