import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from pyvirtualdisplay import Display
import os
import random

def random_wait(base_wait):
    additional_wait = random.randint(0, 5)
    total_wait = base_wait + additional_wait
    time.sleep(total_wait)

def daily_task():
    # Config PyVirtualDisplay
    display = Display(visible=0, size=(1920, 1080))
    display.start()
    # Config Selenium
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--window-size=1920,1080")
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--proxy-server='direct://'")
    chrome_options.add_argument("--proxy-bypass-list=*")
    chrome_options.add_argument("--start-maximized")
    #chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--ignore-certificate-errors')
    chromedriver_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "chromedriver")
    service = Service(chromedriver_path)
    driver = webdriver.Chrome(service=service, options=chrome_options)
    driver.maximize_window()

    # Chargement de la page
    driver.get("https://www.google.com/")
    cookie_consent = driver.find_element(By.ID, "L2AGLb")
    cookie_consent.click()
    search_bar = driver.find_element(By.NAME, "q")
    search_bar.send_keys("lequipe")
    search_bar.send_keys(Keys.ENTER)
    random_wait(2)
    
    # Chargement de la page
    driver.get("https://www.lequipe.fr/")
    random_wait(5)
    # Find the main container element
    main_container = driver.find_element(By.CLASS_NAME, "container")
    articles = []
    # Find all the div elements that contain the articles
    article_divs = main_container.find_elements(By.CLASS_NAME, "FolderWidget__gridItem")
    #article_divs = main_container.find_elements(By.CSS_SELECTOR, "div[class*='ColeaderWidget'][class*='Home__widget'][class*='ColeaderWidget--rightImage']")
    # Loop through each article div and extract the link
    for div in article_divs:
        try:
            link = div.find_element(By.TAG_NAME, "a").get_attribute("href")
            articles.append(link)
        except:
            pass
    for article in articles:
        driver.get(article)
        # Attendre quelques secondes pour simuler la lecture de l'article
        random_wait(2)
        # Revenir à la page précédente
        driver.execute_script("window.history.go(-1)")
        random_wait(5)
    # Close the browser
    driver.quit()
daily_task()