#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import subprocess
import csv

# Répertoire où trouver les logs
root_directory = "/var/log/suricata/"
subfolder = "csv_files"
rarc_file = "./.rarc"

def concatenate_csv(directory, output_file):

    # Création du fichier de sortie final
    with open(output_file, "w", newline="") as output_csv:

        # On définit le même délimiteur que les fichiers CSV
        csv_writer = csv.writer(output_csv, delimiter=";")

        # Booléen pour vérifier si l'entête du fichier CSV a déjà été écrite
        csv_header_written = False

        # On itère sur tous les fichiers du répertoire
        for file_name in os.listdir(directory):

            # On ne prend pas en compte le fichier créé par le script eve2csv.py
            if file_name.endswith(".csv") and file_name != "concatenated.csv":
                file_path = os.path.join(directory, file_name)
                with open(file_path, "r") as input_csv:
                    csv_reader = csv.reader(input_csv, delimiter=";")

                    # On itère sur toutes les lignes du fichier CSV
                    for i, row in enumerate(csv_reader):

                        # On écrit l'entête du fichier CSV une seule fois
                        # Eventuellement on pourrait simplement vérifier si le fichier est vide
                        if i==0 and not csv_header_written:
                            csv_header_written = True
                            csv_writer.writerow(row)

                        # On ne réécrit pas l'entête du fichier CSV en passant la première ligne
                        elif i==0 and csv_header_written:
                            continue

                        # On écrit toutes les autres lignes
                        else:
                            csv_writer.writerow(row)

# Vérifier et créer le dossier subfolder s'il n'existe pas
csv_folder = os.path.join(root_directory, subfolder)
if not os.path.exists(csv_folder):
    os.makedirs(csv_folder)

# On cherche les fichiers PCAP
pcap_files = [file for file in os.listdir(root_directory) if os.path.isfile(os.path.join(root_directory, file)) and "pcap" in file]

# On itère sur tous les fichiers PCAP de la liste pcap_files
for index, pcap_file in enumerate(pcap_files, start=1):

    # Chemin complet du fichier PCAP
    pcap_file_path = os.path.join(root_directory, pcap_file)

    # Chemin complet du fichier Argus
    argus_file_path = os.path.join(root_directory, pcap_file.replace(".pcap", ".argus"))

    # Chemin complet du fichier CSV
    csv_file_path = os.path.join(root_directory, subfolder, pcap_file.replace(".pcap", ".csv"))

    # Check si le fichier CSV existe déjà ou si le PCAP est vide
    if os.path.exists(csv_file_path) or os.path.getsize(pcap_file_path) == 0:
        continue

    # Conversion du fichier PCAP en ficher Argus
    subprocess.run(["/usr/sbin/argus", "-r", pcap_file_path, "-w", argus_file_path])

# On cherche les fichiers Argus
argus_files = [file for file in os.listdir(root_directory) if os.path.isfile(os.path.join(root_directory, file)) and "argus" in file]

# On itère sur tous les fichiers Argus de la liste argus_files
for index, argus_file in enumerate(argus_files, start=1):

    # Chemin complet du fichier Argus
    argus_file_path = os.path.join(root_directory, argus_file)

    # Check si le fichier Argus n'existe pas
    if not os.path.exists(argus_file_path):
        continue

    # Chemin complet du fichier CSV
    csv_file_path = os.path.join(root_directory, subfolder, argus_file.replace(".argus", ".csv"))

    # Options à extraire du fichier Argus, à savoir :
    #  - La durée totale de la connexion
    #  - Le nombre de paquets
    #  - Le nombre de bytes
    #  - Le nombre de bytes (Source -> Destination)
    #  - Le protocole
    #  - Le type de service (Source)
    #  - Le type de service (Destination)
    #  - L'état de la connexion
    #  - Le port de destination
    #  - Le port source
    argus_options = "dur,pkts,bytes,sbytes,proto,stos,dtos,state,dport,sport,dir,stime"

    # Conversion du fichier Argus en CSV
    subprocess.run(["/usr/bin/ra", "-r", argus_file_path, "-c", ";", "-s", argus_options, "-F", rarc_file], stdout=open(csv_file_path, "w"))

    # Supression du fichier Argus
    os.remove(argus_file_path)

# Chemin du dossier où se trouvent les fichiers CSV
destination_dir = os.path.join(root_directory, subfolder)

# On concatène tous les fichiers CSV en un seul : concatenated.csv
concatenate_csv(destination_dir, os.path.join(destination_dir, "concatenated.csv"))