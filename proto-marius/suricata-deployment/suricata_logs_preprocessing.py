import os
import shutil
import subprocess
import csv

"""

-------------------------------------
Script suricata_logs_preprocessing.py
-------------------------------------

Script permettant de récupérer les fichiers Argus et PCAP générés par chaque instance de Suricata. 
L'intérêt est de pouvoir créer une backup de ces fichiers en les copiant en dehors du répertoire synchronisé.
Un CronJob est mis en place côté Suricata pour générer tous les X temps les fichiers Argus associés aux PCAP, en excluant les PCAP en cours de génération.
L'idéal serait d'en mettre également un en place pour ce script sur la machine host afin de traiter les fichiers Argus dès leur création.
Mais la question de la synchronisation se pose entre la machine host et la remote.
Une fois les fichiers Argus et PCAP récupérés, on extraie les features qui nous intéressent et on les stocke dans un fichier CSV correspond.
On concatène en deuxième temps tous les fichiers CSV, en évitant les duplications d'entête. Les performances sont correctes 0(n**2)?, à vérifier s'il ne vaudrait pas mieux
append les fichiers CSV les uns aux autres au fur et à mesure de l'exécution plutôt que de les concaténer à la fin.

"""

# Répertoire où trouver les logs
root_directory = "./logs"

# Répertoire de destination
logs_retrieved_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), "logs_retrieved")

# Création du répertoire de destination s'il n'existe pas
os.makedirs(logs_retrieved_directory, exist_ok=True)

# Playbook suricata-reboot
#ansible_playbook = "suricata-reboot.yml"
#subprocess.run(["ansible-playbook", ansible_playbook, "--ask-become-pass"])

def concatenate_csv(directory, output_file):
    # Création du fichier de sortie final
    with open(output_file, "w", newline="") as output_csv:
        # On définit le même délimiteur que les fichiers CSV
        csv_writer = csv.writer(output_csv, delimiter=";")
        # Booléen pour vérifier si l'entête du fichier CSV a déjà été écrite
        csv_header_written = False
        # On itère sur tous les fichiers du répertoire
        for file_name in os.listdir(directory):
            # On ne prend pas en compte le fichier créé par le script eve2csv.py
            if file_name.endswith(".csv") and file_name != "suricata.csv" and file_name != "concatenated.csv":
                file_path = os.path.join(directory, file_name)
                with open(file_path, "r") as input_csv:
                    csv_reader = csv.reader(input_csv, delimiter=";")
                    # On itère sur toutes les lignes du fichier CSV
                    for i, row in enumerate(csv_reader):
                        # On écrit l'entête du fichier CSV une seule fois
                        # Eventuellement on pourrait simplement vérifier si le fichier est vide
                        if i==0 and not csv_header_written:
                            csv_header_written = True
                            csv_writer.writerow(row)
                        # On ne réécrit pas l'entête du fichier CSV en passant la première ligne
                        elif i==0 and csv_header_written:
                            continue  
                        # On écrit toutes les autres lignes
                        else:
                            csv_writer.writerow(row)


# On cherche tous les répertoires suricata, les sous-répertoires du répertoire racine
subdirectories = [subdir for subdir in os.listdir(root_directory) if os.path.isdir(os.path.join(root_directory, subdir))]

# On itère sur tous les sous-répertoires de la liste subdirectories
for subdir in subdirectories:

    # Chemin complet du sous-répertoire courant
    dir_path = os.path.join(root_directory, subdir)

    # On cherche les fichiers Argus
    argus_files = [file for file in os.listdir(dir_path) if os.path.isfile(os.path.join(dir_path, file)) and "argus" in file]

    # Répertoire de destination pour les fichier PCAP et Argus
    destination_dir = os.path.join(logs_retrieved_directory, subdir)
    os.makedirs(destination_dir, exist_ok=True)

    # Copie du fichier eve.json dans le répertoire de destination
    fast_log_file_path = os.path.join(dir_path, "fast.log")
    if not os.path.isfile(os.path.join(fast_log_file_path)):
        with open(fast_log_file_path, "w") as fast_log_file:
            fast_log_file.write("")
        #shutil.copy(fast_log_file_path, destination_dir)
    
    # On itère sur tous les fichiers Argus de la liste argus_files
    for index, argus_file in enumerate(argus_files, start=1):
        
        # Chemin complet du fichier Argus
        argus_file_path = os.path.join(dir_path, argus_file)
        # Chemin complet du fichier PCAP associé, on évite de la même façon les PCAP en cours de génération puisqu'ils ne sont pas pris en compte dans pcap2argus.py
        pcap_file_path = os.path.join(dir_path, argus_file.replace(".argus", ".pcap"))

        # On check si le PCAP n'a pas déjà été copié dans le répertoire de destination, auquel cas on skip
        if os.path.isfile(os.path.join(destination_dir, os.path.basename(pcap_file_path))):
            continue

        # Copie des fichiers Argus et PCAP dans le répertoire de destination
        shutil.copy(argus_file_path, destination_dir)
        shutil.copy(pcap_file_path, destination_dir)

        # Chemin complet du fichier CSV
        csv_file_path = os.path.join(destination_dir, argus_file.replace(".argus", "") + ".csv")

        # Options à extraire du fichier Argus, à savoir :
        #  - La durée totale de la connexion
        #  - Le nombre de paquets
        #  - Le nombre de bytes
        #  - Le nombre de bytes (Source -> Destination)
        #  - Le protocole
        #  - Le type de service (Source)
        #  - Le type de service (Destination)
        #  - L'état de la connexion
        #  - Le port de destination
        #  - Le port source
        argus_options = "dur,pkts,bytes,sbytes,proto,stos,dtos,state,dport,sport,dir"

        # Conversion du fichier Argus en CSV
        subprocess.run(["ra", "-r", argus_file_path, "-c", ";", "-s", argus_options], stdout=open(csv_file_path, "w"))

        # On analyse le fichier CSV pour détecter les connexions SSH
        with open(csv_file_path, "r") as csv_file:
            lines = csv_file.readlines()

        for line_number, line in enumerate(lines):
            # On ignore la première ligne qui contient l'entête
            if line_number == 0:
                continue
            fields = line.strip().split(";")
            # On récupère le port de destination
            dport = fields[8]
            print("Destination port : " + str(dport))
            # On vérifie si le port de destination est le port SSH
            if dport == "ssh":
                # On récupère le nombre de bytes totaux et le nombre de bytes reçus
                bytes_tot = int(fields[2])
                bytes_received = int(fields[3])
                print("Bytes B->A : " + str(bytes_tot - bytes_received))
                # Pour vérifier qu'il s'agit bien d'une connexion SSH réussie, on vérifie que le nombre de bytes totaux - reçus est supérieur au seuil donné
                if bytes_tot - bytes_received > 5000:
                    print("SSH successful login attempt detected")
                    log_message = "06/28/2023-08:58:41.501210  [**] [1:1000008:1] SSH login attempt [**] [Classification: (null)] [Priority: 3] {TCP}"
                    # On écrit le log dans le fichier fast.log
                    with open(os.path.join(destination_dir, "fast.log"), "a") as fast_log_file:
                        fast_log_file.write(log_message + "\n")
        
    # Script prétraitement du fichier eve.json, afin de récupérer les events netflow
    subprocess.run(["python3", "eve2csv.py", dir_path, destination_dir])

    # On concatène tous les fichiers CSV en un seul : concatenated.csv
    concatenate_csv(destination_dir, os.path.join(destination_dir, "concatenated.csv"))

# Fonction pour supprimer les fichiers inutiles, à savoir tous les fichiers Argus
def delete_files(subdir, excluded_files):
    subdir_path = os.path.join(subdir)
    # On itère sur tous les éléments du répertoire
    for file_name in os.listdir(subdir_path):
        # On s'assure qu'il s'agit bien d'un fichier
        if os.path.isfile(os.path.join(subdir_path, file_name)):
            file_path = os.path.join(subdir_path, file_name)
            # On supprime le fichier s'il ne fait pas partie des fichiers à conserver, c'est-à-dire les fichiers issus du prétraitement
            if file_name not in excluded_files and ".pcap" not in file_name and ".csv" not in file_name:
                os.remove(file_path)

# Répertoire où supprimer les fichiers
directory = "./logs_retrieved"

# Fichiers à conserver
excluded_files = ["suricata.csv", "concatenated.csv", "fast.log"]

# Suppression des fichiers par appel de delete_files pour chaque sous-répertoire
for subdir in os.listdir(directory):
    subdir_path = os.path.join(directory, subdir)
    delete_files(subdir_path, excluded_files)