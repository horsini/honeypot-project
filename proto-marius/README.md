# proto-marius

🚧 WIP 🚧

## ⚡ Project goal ?
The project aims to deploy honeypots to capture network traffic and collect data in order to build a dataset on current botnet trends.

## 📂 Directories :

* [cowrie-deployment](/proto-marius/cowrie-deployment/) This repository contains an Ansible playbook to automate the installation and configuration of the Cowrie honeypot on a target system.

* [suricata-deployment](/proto-marius/suricata-deployment/) This repository contains every files to set up isolated networks that includes a vulnerable virtual machine and a Suricata VM that captures network traces. ([README](https://gitlab.inria.fr/horsini/honeypot-project/-/blob/main/proto-marius/suricata-deployment/README.md))

* [false-life](/proto-marius/false-life/) This directory contains a collection of Python scripts designed to simulate various aspects of life on a virtual machine. These scripts aim to create a virtual environment that mimics real-life scenarios and interactions within a virtualized system.
