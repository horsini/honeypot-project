#set text(
    size: 11pt
)
#set par(
  justify: true
)
#set heading(numbering: "I.1")

#let title = [
  Déploiement d'Honeypots - \ Rapport de stage
]

#let year = [
  Année 2022/2023
]

#show link: underline

#table(
columns: (22%, 22%, 50pt ,24%, 22%), inset: 8pt, align: horizon, image("logos/esir.png"), 
image("logos/Logo-Universite-de-Rennes-1.png"),[],image("logos/Logo-CentraleSupélec.svg.png"), image("logos/inria.png"),
stroke: white )

#table(
  columns : (50%, 50%), inset : 8pt, align : horizon, [*Marius Le Douarin* \ TI - Systèmes d'Information \ 2023 \ \ *Manal Hamzaoui* \ Enseignante-chercheuse], [*CentraleSupélec* \ 3 rue Joliot-Curie, 91192 GIF-SUR-YVETTE \ communication$@$centralesupelec.fr \ \ *Hélène Orsini* \ Doctorante]
)

#v(110pt)

#align(center, text(30pt)[#title])

#v(260pt)

#align(left, text(17pt)[#year])

#pagebreak()

#set page(
    numbering : "1",
    number-align : right)

#align(left, text(17pt)[*Résumé*])

#v(10pt)

L'objectif de ce stage était de mettre en place les moyens nécessaires pour permettre la capture de traces réseaux d'attaques dans le cadre de la thèse d'Hélène Orsini, doctorante à CentraleSupélec (Rennes) dans l'équipe CIDRE (_Confidentiality, Integrity, Disponibilité, Repartition_) dont l'intitulé est "_Machine Learning Pipeline for Network Attack Incident Detection and Classification_". En effet, l'une des principales limites pour les doctorants et chercheurs dans le domaine de la détection d'intrusion est le manque de datasets réalistes. Les datasets les plus utilisés actuellement sont généralement anciens et perfectibles. L'intérêt, au travers de ce stage, était donc de mettre en place un système permettant de capturer de réelles traces réseaux d'attaques au travers d'une méthode de défense active en cybersécurité : les honeypots.

Ces honeypots sont en réalité des leurres qui ont pour but d'attirer des cybercriminels sur des pseudo-systèmes, qu'ils pensent être réels, tout en les détournant de véritables ressources sensibles. Grâce à ces honeypots, il est possible de cerner des tendances chez ces attaques comme les techniques employées ou bien les intentions qui en découlent. @hp

Pour concentrer ces attaques, les honeypots sont configurés de telle sorte à être exposés sur Internet, et à mettre en avant des services vulnérables. Ces services sont généralement des services très utilisés, comme des serveurs web, des serveurs de messagerie, etc. Il y a eu dans un premier lieu cette phase de réflexion sur les services qui seraient susceptibles d'attirer le plus d'attaques possibles, et donc de nous assurer une quantité de données satisfaisantes pour la phase d'analyse d'Hélène. Dans un second temps, nous avons dû mettre ces hypothèses en pratique en déployant ces honeypots grâce au travail d'Alexandre Sanchez, ingénieur-chercheur dans l'équipe CIDRE, et d'adapter tout au long de la phase d'expérimentation le mode de capture de ces honeypots pour que les données correspondent du mieux que possible aux besoins.

#v(10pt)

#align(left, text(17pt)[*Abstract*])

#v(10pt)

The objective of this internship was to establish the necessary means to enable the capture of network attack traces within the scope of the doctoral research of Hélène Orsini, PhD student at CentraleSupélec (Rennes) in the CIDRE team, titled "Machine Learning Pipeline for Network Attack Incident Detection and Classification". Indeed, one of the main challenges for PhD students and researchers in intrusion detection is the lack of realistic datasets. Currently, the most commonly used datasets are typically outdated and improvable. The aim of this internship, therefore, was to implement a system capable of capturing genuine network attack traces through an active cybersecurity defense method: honeypots.

Honeypots are, in essence, decoys designed to lure cybercriminals into pseudo-systems they believe to be real while diverting them from actual sensitive resources. These honeypots allow the identification of attack trends, such as employed techniques and underlying intentions.

To attract these attacks, the honeypots are configured to be exposed on the Internet, emphasizing vulnerable services. Typically, these services are widely used, such as web servers, mail servers, etc. Consequently, there was an initial phase of reflection to determine the services most likely to attract a substantial number of attacks, thus ensuring a satisfactory quantity of data for Hélène's analysis phase. Subsequently, we put these hypotheses into practice by deploying these honeypots, thanks to the efforts of Alexandre Sanchez, an engineer-researcher in the CIDRE team, and adapt the honeypots' capture mode throughout the experimentation phase to make the data best meet the needs.

#pagebreak()

#outline(
    title: text(17pt)[*Sommaire* #v(10pt)],
    indent: auto)

#pagebreak()

#set page(header: [
    #set text(11pt) 
    #set align(left)
    _Déploiement d'Honeypots_
    #h(1fr)
    *_Introduction_*
    ])

= Introduction

#v(10pt)

L'avènement de l'ère du numérique a révolutionné notre façon de vivre, de travailler, et de communiquer. Cette transition vers la digitalisation nous a plongé dans un monde où les systèmes informatiques sont omniprésents dans notre quotidien, et dont beaucoup sont basés sur des dispositifs massivement interconnectés. Cette multiplication d'interfaces connectées a augmenté le nombre de surfaces d'attaques et leurs lots de vulnérabilités. La sécurité informatique, aussi connue sous le nom de cybersécurité, est devenue un enjeu majeur pour répondre aux besoins de défenses, de compréhensions des attaquants tout en assurant la sécurité globale. 

Les intentions derrière les cyberattaques sont diverses et variées, et lorsqu'un attaquant obtient un accès non autorisé à un système, ce dernier devient un véritable terrain de jeu pour lui. Dans les activités les plus courantes, nous retrouvons : 

- le vol de données personnelles/sensibles (pour de l'usurpation d'identité, du chantage, où l'accès à des comptes en ligne en obtenant des identifiants et des mots de passe)

- le déni de service (DDoS) visant à inonder la cible d'attaques entraînant une surcharge interne et une indisponibilité du service

- le minage de crypto monnais en exploitant les ressources du système notamment les capacités du processeur

- l'upload de contenu pour des réseaux illégaux

- le déploiement de logiciels malveillants comme des ransomwares permettant de chiffrer les données du système cible et demander une rançon en échange de la clé de déchiffrement, etc.

La cybersécurité c'est l'ensemble des mesures, pratiques et technologies visant à protéger ces systèmes informatiques, les réseaux, les données et les applications contre des attaques malveillantes et empêcher des accès non autorisés. Elle vise à assurer la confidentialité, l'intégrité et la disponibilité des informations numériques, ainsi qu'à prévenir et à détecter les cybermenaces.

L'ANSSI (_Agence Nationale de la Sécurité des Systèmes d'Information_) propose une définition plus élaborée : "État recherché pour un système d'information lui permettant de résister à des évènements issus du cyberespace susceptibles de compromettre la disponibilité, l'intégrité ou la confidentialité des données stockées, traitées ou transmises et des services connexes que ces systèmes offrent ou qu'ils rendent accessibles. La cybersécurité fait appel à des techniques de sécurité des systèmes d'information et s'appuie sur la lutte contre la cybercriminalité et sur la mise en place d'une cyberdéfense.". @cyber

La cybersécurité est un domaine tellement vaste qu'elle doit être considérée sous différents aspects :

- À petite échelle, elle concerne les individus, et vise à protéger les données personnelles avec la prolifération des données numériques qui constituent désormais une part importante de notre vie privée.

- À moyenne échelle, elle concerne des attaques contre des infrastructures critiques telles que l'énergie, les transports, les services publics, les services de santé (comme nous avons pu récemment le constater avec l'intrusion du CHU de Rennes). Ces attaques peuvent mettre en péril la société et l'économie. Ou bien, elle concerne la sécurité des entreprises, entrainant la perte de propriété intellectuelle, la perturbation des opérations commerciales et la perte de confiance des clients si leurs données sont compromises.

- À une échelle plus importante, elle concerne les États-nations avec le cyberespionnage/cyberterrorisme dont le but est de voler des informations confidentielles, notamment en tant de guerre.

Protéger les différents fronts de la cybersécurité est une tâche complexe, et c'est pourquoi il est nécessaire de mettre en place des stratégies de défense adaptées à chaque échelle. Ces stratégies de défense reposent généralement sur des modèles de sécurité permettant de décrire les différents aspects de la sécurité d'un système. Ces modèles aident à identifier les vulnérabilités, à établir des politiques de sécurité et à concevoir des mécanismes de protection efficaces.

Il faut également garder en tête que les méthodes d'attaques évoluent constamment, elles exploitent des failles de plus en plus techniques et sont mieux organisées. Cela implique ainsi la nécessité pour la défense de s'adapter en permanence pour pouvoir faire face à ces nouvelles menaces émergentes.

#pagebreak()

#set page(header: [
    #set text(11pt) 
    #set align(left)
    _Déploiement d'Honeypots_
    #h(1fr)
    *_CentraleSupélec - Équipe CIDRE_*
    ])

= CentraleSupélec - Équipe CIDRE

#v(10pt)

Le stage s'est déroulé sur le site de la technopole Rennes Atalante, dans les locaux de CentraleSupélec, école supérieure spécialisée dans les métiers du numérique et de la technologie de pointe, et plus particulièrement au sein de l'équipe CIDRE. CIDRE est une équipe de recherche commune à CentraleSupélec, à Inria, à l'Université de Rennes 1 et au CNRS, et est directement rattachée à l'IRISA, l'_Institut de Recherche en informatique et Systèmes Aléatoires_. @cidre @cidre2

Nous avons pu constater, lors de l'introduction, tous les enjeux que l'ère du numérique a pu engendrer et c'est pourquoi l'objectif pour l'équipe CIDRE, spécialisée dans la sécurité informatique, est d'améliorer le niveau de sécurité des systèmes largement déployés, de fournir des systèmes opérationnels capables de résister à des évènements malveillants et de respecter la vie privée de leurs utilisateurs. 

Il y a donc trois aspects d'ordre pour cette équipe, d'un point de vue sécurité, à savoir : la confiance, la détection d'intrusion et la confidentialité. La compréhension et détection d'intrusion est mise en place sur différents fronts et à différents niveaux :

- Une partie de l'équipe travaille plutôt sur de l'hardware (matériel) qui est vouée à changer de nom pour devenir l'équipe SUSHI.

- L'autre partie de l'équipe travaille plutôt sur du software (OS, logiciel) prochainement l'équipe PIRAT. Cette partie inclut un petit groupe qui travaille avec de l'IA pour essayer de modéliser des comportements. 

C'est dans ce même environnement qu'Hélène Orsini, tutrice de mon stage et doctorante en seconde année de thèse, évolue. Elle est encadrée par deux chercheurs de l'équipe : Valérie Viet Triem Tong professeur et experte cyber et Yufei Han chercheur et expert IA, sur son sujet qui se nomme "_Machine Learning Pipeline for Network Attack Incident Detection and Classification_".

Cette thèse s'incrit dans le domaine de la détection d'intrusion. La détection d'intrusion peut s'effectuer à trois niveaux différents : OS (Système d'exploitation), Réseau et Applications. Pour ces niveaux, il y a deux types d'analyses possibles :

- Une analyse basée sur des règles/signatures où on implémente des règles en fonction des attaques qu'on connait pour les détecter. Cette analyse présente néanmoins une limite : il est impossible de détecter les attaques qui n'ont pas été répertoriées au préalable.

- Une analyse comportementale basée sur du Machine Learning, une approche plus avancée, qui vise à identifier les menaces potentielles en observant le comportement du trafic réseau ou des utilisateurs plutôt que de se fier uniquement à des signatures spécifiques d'attaques connues. Les points forts de cette analyse sont donc la détection d'attaques méconnues, l'adaptabilité aux menaces émergentes, et une diminution des faux positifs. Cependant, cette approche reste expérimentale notamment à cause de l'enjeu de la confiance accordée à l'IA appliquée à la "vraie vie".

En d'autres termes, son objectif est de permettre la détection d'intrusion au niveau réseau avec du Machine Learning, avec une première phase de nettoyage en choisissant les données que l'on souhaite prendre en compte, une seconde phase d'embedding des données, une troisième phase de détection des activités malicieuses vis-à-vis des activités bénignes, puis une dernière phase de classification de ces données par clusterisation selon les différents types d'attaques qui existent.

Ces modèles de Machine Learning ont cependant besoin d'être approvisionnés en données, dans le cadre d'un apprentissage supervisé, pour garantir des résultats satisfaisants et les plus justes possibles. Ou bien comme dans le cas d'Hélène, en apprentissage non supervisé, pour avoir une référence en obtenant des labels sur les données extraites pour pouvoir comparer les résultats finaux issus de ses modèles afin de crédibiliser et d'ajouter une plus-value aux expériences avancées lors de sa thèse.

Mon objectif était donc d'apporter mon aide dans son travail, en lui fournissant des données exploitables par ses modèles, construits au préalable, pour assurer une phase d'expérimentation la plus réaliste possible, et donc de pouvoir tirer des conclusions pertinentes sur la thématique.

L'idée ici était de mettre en place des machines vulnérables, exposées sur Internet, afin d'attirer des attaques qui devront alors être traitées et interprétées pour pouvoir être exploitées par ces modèles de Machine Learning.

Cette tâche peut cependant s'annoncer plus délicate que prévu. Effectivement, collecter de telles données en situation réelle peut s'avérer être une tâche complexe puisqu'elle nécessite une interaction avec des entités externes que sont les attaquants. 

Il faut donc penser une stratégie en amont permettant d'exposer ces machines avec un certain nombre de vulnérabilités dévoilées aux yeux de tous, les rendant ainsi détectables et atteignables.

C'est à partir de là que Alexandre Sanchez, ingénieur-chercheur de l'équipe CIDRE, nous aura été d'une aide essentielle. De fait, les expérimentations menées par Hélène nécessitent d'être correctement encadrées, puisqu'elles soulèvent des considérations éthiques et légales. Alexandre est là pour nous garantir un accès à Hoplab, une plateforme sécurisée et monitorée, séparée de l'intranet de l'Inria, et qui nous a permis d'y héberger de telles machines.

#pagebreak()

#set page(header: [
    #set text(11pt) 
    #set align(left)
    _Déploiement d'Honeypots_
    #h(1fr)
    *_Analyse_*
    ])

= Analyse

#v(10pt)

== Contexte

#v(5pt)

Comme énoncé précédemment, le contexte du problème réside dans la nécessité de collecter des données réelles d'attaques réseaux, pour permettre à Hélène de les exploiter dans le cadre de sa thèse. Ce problème est d'ailleurs l'un des défis majeurs auxquels sont confrontés les doctorants et chercheurs en cybersécurité, ils ont besoin de jeux de données réalistes et complets pour mener à bien leurs expériences. Malgré les avancées qui ont pu être faites dans ce domaine, les jeux de données publics disponibles se comptent sur les doigts d'une main, surtout par souci de confidentialité des informations. Nous retrouvons notamment, dans le lot, le très convoité CICIDS2017. Bien que ce jeu de données soit basé sur du trafic réseau, écouté en temps réel, il a été capturé dans un environnement contrôlé qui porte le nom de testBED (_Un testBED est « une plateforme permettant d'effectuer des tests rigoureux, transparents et reproductibles de théories scientifiques, d'outils informatiques et de nouvelles technologies »_ - selon Wikipédia @testbed), où il s'agissait de simuler et reproduire la complexité et la dynamique d'un réseau dans son intégralité.

Malheureusement, une telle complexité est compliquée à penser et à imiter à la perfection. Cela a notamment fait l'oeuvre d'un papier de recherche, « Errors in the CICIDS2017 dataset and the significant differences in detection performances it makes » rédigé par Maxime Lanvin, Pierre-François Gimenez, Yufei Han, Frédéric Majorczyk, Ludovic Mé, Eric Totel, membres de l'équipe CIDRE. @cic

Ce papier stipule l'existence de mécanismes de détection d'intrusion qui vise à identifier des attaques par l'écoute du trafic réseau, appelés : _Network Intrusion Detection Systems (NIDS)_. Ces outils permettent la capture du trafic brut. C'est ce même trafic qui est généralement exploité par des outils tels que CICFlowMeter afin de créer des flux de données (des conversations sur le réseau) qui sont, la plupart du temps, labellisés et donc exploitables par différents modèles de Machine Learning.

Le papier entre alors dans les détails en expliquant les erreurs du jeu de données CICIDS2017 qui ont été descellées lors de la conversion du trafic brut en flux de données grâce à CICFlowMeter. Nous retrouvons notamment :

- Des attaques de scan de ports qui ont été incorrectement labellisées. Pour définir la notion de scan de ports : d'un point de vue réseau, les paquets de communication entre deux entités ont besoin d'être acheminés d'un point A (avec une adresse IP A sur le réseau) et un point B (avec une adresse IP B sur le réseau) pour que les deux puissent interagir. Pour cela, des protocoles comme TCP ou UDP, s'en chargent et définissent au passage le concept de ports sur un ordinateur @port. Une application (ou un service) peut envoyer du trafic et écouter sur un port particulier d'une machine. La combinaison d'une adresse B de destination et d'un port particulier permet aux périphériques de routage de s'assurer que le trafic circule comme prévu. Un scan de port est une technique utilisée par les attaquants pour identifier les ports ouverts sur un système. Cela permet à l'attaquant de cartographier les services et les applications qui s'exécutent sur un système, et de déterminer les vulnérabilités qui pourraient être exploitées. La moindre erreur de labellisation peut donc avoir des conséquences cruciales sur les résultats d'expérimentation.

- Il s'est aussi avéré que CICIDS2017 comportait des problèmes de capture puisqu'une bonne partie du trafic réseau brut est dupliquée, entraînant des problèmes d'extraction des features et de labellisation des données pour l'apprentissage supervisé.

#pagebreak()

Finalement, ce papier était une manière de mettre en avant les limites du jeu de données CICIDS2017, jugé en partie obsolète, et donc indirectement de pointer du doigt certaines références de la littérature scientifique sur ce sujet, où les modèles utilisés lors d'expériences avec ce jeu de données présentaient des résultats avec de très bonnes valeurs de rappel. _« Le rappel est une métrique d'évaluation d'un modèle qui décrit le pourcentage d'échantillons de données qu'un modèle d'apprentissage identifie correctement comme appartenant à une classe d'intérêt, la classe "positive", sur le nombre total d'échantillons pour cette classe »_. @recall

Mettre en avant un taux de rappel important en sachant la présence avérée de faux positifs dans le jeu de données, qui n'ont pas été relevés par les personnes ayant réalisé les expériences, est donc une erreur, et peut décrédibiliser les conclusions avancées.

À noter que ce papier n'a pas pour seul but de mettre en avant les problèmes du jeu de données CICIDS2017, mais aussi d'énoncer des propositions de corrections et d'améliorations qui pourraient permettre aux futurs jeux de données émergents d'être plus complets, voire plus réalistes.

La nécessité de construire un nouveau jeu de données paraît donc évidente.

== État de l'art

#v(5pt)

Lors de la mise en contexte, nous avons mis en avant l'existence des testBEDs qui sont, grossièrement résumés, des bacs à sable où nous pouvons définir nous-même les règles du jeu, et ainsi pouvoir contrôler les attaques qui seront menées sur nos machines de test.

J'ai pu prendre connaissance et comprendre l'intérêt de ces testBEDs puisqu'en parallèle de mon stage deux étudiants effectuaient le leur sur cette thématique. Ils ont pu me partager leur retour d'expérience lors d'une réunion instructive à ce sujet.

Ils travaillaient sur SOCBED, _« un testBED d'expérimentation de cyberattaques open source autonome qui utilise des machines virtuelles pour simuler le réseau d'une petite entreprise, y compris l'activité bénigne des utilisateurs, diverses cyberattaques et une collection centrale de données de journal »_. @socbed

Encore une fois, ce type d'environnement est très pratique pour avoir un total contrôle sur les opérations qui se déroulent, et sur la qualité des flow finaux (post-traitement par CICFlowMeter) correctement labellisés. Cependant, la partie de l'attaque peut éventuellement manquer de diversité, dû au biais inconscient que nous pouvons avoir lors de la définition des règles du jeu. 

Par exemple, pour reprendre le cas du jeu de données CICIDS2017, il a été constaté que les machines mises en place pour effectuer des attaques étaient identiques en tout point. Répliquer une même configuration implique par exemple une entête (une forme de signature faite par la machine) similaire dans les paquets d'attaques, rendant la distinction entre les activités malicieuses et bénignes triviales, ce qui ne devrait pas avoir lieu.

C'est pourquoi, dans la situation d'Hélène, l'intérêt était de ne pas avoir le contrôle sur la partie attaque, pour éviter d'intégrer ce genre d'erreurs qui sont, à première vue, insignifiantes mais qui peuvent compromettre l'intégralité d'un jeu de données. L'autre intérêt était de laisser de vrais attaquants mener leurs propres tentatives d'intrusion pour envisager une diversité d'attaques plus importante.

Pour remplir cette mission, après un travail de recherche en amont du stage et de multiples réunions de brainstorming avec mes encadrants de stage, j'ai fait la connaissance des pots de miel, plus communément appelés _honeypots_.

_« Un honeypot est un mécanisme de cybersécurité qui utilise une cible d'attaque fabriquée de toutes pièces pour éloigner les cybercriminels de cibles légitimes et qui recueille également des renseignements sur l'identité, les méthodes et les motivations des cyber adversaires._

_Un honeypot peut être calqué sur une ressource numérique, comme une application logicielle, un serveur ou le réseau lui-même. Il est conçu délibérément et intentionnellement pour ressembler à une cible légitime en termes de structure, de composants et de contenu. L'objectif est de convaincre les intrus qu'ils ont obtenu un accès au véritable système et de les encourager à passer du temps dans cet environnement que nous contrôlons. »_ @hp

Les attaquants, dans le cadre de leurs attaques, font la plupart du temps usage de botnets. Les botnets sont des réseaux d'ordinateurs infectés par des logiciels malveillants qui sont sous le contrôle d'une seule partie attaquante. Chaque ordinateur infecté est appelé un bot, et est généralement utilisé pour effectuer des tâches automatisées, indépendamment de la volonté de son propriétaire. Ces botnets peuvent être plus ou moins gros en fonction de la cible visée. @botnet 

Le honeypot sert de leurre pour détourner les cybercriminels de véritables cibles, mais également d'outil de reconnaissance pour évaluer les techniques, les capacités et le degré de sophistication des botnets sur la base de leurs tentatives d'intrusion.

Il s'est avéré, lors d'une de ces réunions, qu'un doctorant de l'équipe CIDRE, Pierre-Victor Besson, travaillait actuellement sur son sujet de thèse qui porte sur la mise en place de tels honeypots, il a donc pu nous partager son expérience et ses connaissances sur le sujet. 

Pierre-Victor utilise des honeypots du nom de _Cowrie_, qui sont des honeypots SSH (protocole permettant d'établir une communication chiffrée entre une machine local et une machine distante) et Telnet (protocole similaire sur le principe) écrit en Python. Il est conçu pour être facile à installer et à mettre à jour. Il faut cependant garder une chose en tête lorsque nous souhaitons configurer des honeypots, il existe différents niveaux d'intéractions qu'un attaquant peut avoir avec ces derniers @thm :

- Low-Interaction : Le honeypot est seulement capable de simuler un service et capturer des attaques. Cependant les attaquants ne peuvent pas interagir avec le système d'exploitation sous-jacent (celui sur lequel il est installé).

- Medium-Interaction : Le honeypot est capable de simuler à la fois les services vulnérables, ainsi que les systèmes d'exploitation, le shell (permettant de rédiger des lignes de commande) et un système de fichiers. Cependant, une nouvelle fois, il ne s'agit que d'une simulation. Typiquement ces honeypots créent, grâce à Python, un environnement virtuel, séparé du système d'exploitation hôte, sur lequel les attaquants pourront effectuer des opérations très basiques mais également très limitées car il ne s'agit que d'une simulation.

Dans ces deux cas, l'intérêt est de réduire les risques de compromission du système d'exploitation sous-jacent, tout en n'éloignant pas la possibilité de capturer des attaques entrantes.

- High-Interaction : Ici, les honeypots sont généralement des machines virtuelles (_« Une machine virtuelle est un environnement virtualisé qui fonctionne sur une machine physique. Elle exécute son propre système d'exploitation et bénéficie des mêmes équipements qu'une machine physique : CPU, mémoire RAM, disque dur et carte réseau »_ @vm). 

L'intérêt dans ce dernier cas est d'avoir une machine avec toutes les vulnérabilités. Les attaquants peuvent théoriquement effectuer ce qu'ils souhaitent car il s'agit d'un système complet. Cependant, il faut garder en tête que le risque de compromission est très élevé, et qu'il faut donc être très vigilant lors de la mise en place de tels honeypots pour éviter de créer un point d'entrée pour une attaque vers un réseau complet (par exemple celui d'une entreprise).

Les travaux de Pierre-Victor reposent sur des honeypots de type Medium-Interaction. Nous avons pu nous rendre compte que les objectifs de chacun étaient disparates. Hélène avait effectivement besoin des traces réseaux, mais elle était également intéressée par ce que nous appelons l'activité système, c'est-à-dire les commandes effectuées par les attaquants lorsqu'ils s'introduisent sur ces honeypots, pour comprendre le but de la manœuvre.

Dans le cas des honeypots de type Low ou Medium-Interaction des botnets peu sophistiqués chercheront seulement à lancer une liste de tâches, qu'ils ne pourront pas exercer pleinement. Dans ce genre de contexte, les botnets plus élaborés peuvent rapidement se rendre compte qu'ils sont face à un honeypot, et donc se savoir écoutés. À partir de là, ils n'ont aucun intérêt à poursuivre leurs attaques, au risque d'en dévoiler plus sur leurs activités, plutôt que d'y gagner réellement quelque chose. Or c'est justement ces pratiques qui nous intéressent, et qui peuvent nous permettre de nous adapter à l'avenir pour attirer d'autant plus d'attaques.

Au contraire, sur des honeypots de type High-Interaction, la distinction entre un leurre et une cible réelle est plus compliquée à cerner, permettant de leurrer des botnets plus sophistiqués, en offrant notamment une plus grande surface d'attaque. Les honeypots High-Interaction permettent aux attaquants de mener des actions plus avancées en offrant un accès interactif attrayant similaire à celui d'un véritable système.

C'est pourquoi nous ne pouvions pas nous contenter de simples honeypots de type Medium-Interaction, il nous fallait plutôt des machines virtuelles vulnérables, qui demanderont un travail supplémentaire de supervision, mais qui sont bien plus alléchantes pour des individus malveillants.

== Méthodologie et plan de travail

#v(5pt)

La méthode et le plan de travail se sont dessinés au fur et à mesure de l'avancement du stage. En effet, il était difficile d'établir dès le début toutes les étapes à suivre, la recherche nous pousse dans un premier temps à effectuer un état de l'art pour évaluer les solutions existantes. C'est en prenant connaissance de ces solutions que nous pouvons penser plus efficacement et évaluer les différentes possibilités qui s'offrent à nous.

Cet état de l'art était une étape essentielle dans la démarche de recherche puisqu'elle a complété les connaissances acquises lors de mon cursus, d'un point de vue détection d'intrusion sur des systèmes. La culture générale sur la thématique de la cybersécurité m'a tout de même permis d'avoir une base de connaissances, qu'il a fallu rapidement approfondir pour m'adapter au mieux à ce nouvel environnement spécifique qu'est la détection d'intrusion.

Ensuite des réunions avec les encadrants de thèse d'Hélène, Valérie Viet Triem Tong et Hufei Han, ainsi que Alexandre Sanchez, se sont mises en place très rapidement. C'était une manière, au delà du sujet global du stage, d'être en phase et réfléchir aux objectifs qu'il fallait atteindre à court terme, puis à long terme, pour correspondre au mieux aux attentes d'Hélène pour son travail d'expérimentation.

Dans un premier temps, une phase de réflexion approfondie a été entreprise afin de définir les types d'activités malveillantes à détecter. Cette étape a été cruciale pour cibler les scénarios d'attaques réalistes et émergents, ce qui garantissait que le jeu de données serait représentatif des menaces actuelles et potentielles.

La réflexion quant aux challenges fixés autour des honeypots était partie intégrante de la méthodologie de travail à adopter. 
Il y avait trois points essentiels autour desquels la réflexion devait s'articuler :

- La question du stockage des données ou comment gérer la collecte des données, à quelle fréquence, et où les entreposer pour les futures analyses.

- La question de la sécurisation des honeypots ou comment assurer un contrôle total des machines pour ne pas être impliqué dans des activités éthiquement répréhensibles voire illégales.

- La question de la crédibilisation des honeypots par l'intermédiaire de la création de la fausse vie.

Nous verrons plus tard, en détail, comment ces enjeux ont été considérés et comment les problématiques qui y sont liées ont été endiguées.

La mise en place d'un honeypot haute interaction est devenue le cœur du plan de travail, et ce, en étroite collaboration avec Alexandre qui était notre porte d'entrée pour la plateforme Hoplab mais qui a aussi contribué à la réflexion sur l'aspect sécurisation des machines. Cette collaboration m'a aussi valu une visite de la salle serveur du laboratoire de l'Inria. 

J'ai également eu la chance d'être suivi très régulièrement par Hélène qui s'est souvent rendue disponible sur son temps de travail pour discuter de l'avancement de mes tâches et pour me donner de précieux conseils quant aux pistes de réflexion à suivre d'une étape à l'autre, et ce, grâce à ses connaissances et son expertise dans le domaine. La fréquence de ces temps de discussion s'élevait à trois fois par semaine en moyenne, ce qui m'a permis de ne jamais rester bloqué trop longtemps sur un problème, et de pouvoir avancer efficacement. Ces suivis m'ont paru essentiels pour donner de l'importance au travail que je fournissais, et je l'en remercie pour ça.

À noter que dans la suite de ce rapport, les honeypots seront appelés poneypots. C'est le nom que nous avons choisi de donner au projet. L'origine résulte dans le fait que nous cherchions un nom cohérent lors d'une réunion, et Valérie Viet Triem Tong avait proposé le nom de « poney » sur le ton de l'humour, puis il s'est vite avéré que ce nom était plutôt sympathique et qu'il convenait à tout le monde dû à sa similitude avec le mot « honey » donc nous avons fait le choix de garder le nom « Poneypot ».

#pagebreak()

#set page(header: [
    #set text(11pt) 
    #set align(left)
    _Déploiement d'Honeypots_
    #h(1fr)
    *_Application_*
    ])

= Application

#v(10pt)

== Solution proposée

#v(5pt)

L'idée est donc d'installer des machines sur Hoplab, et de les ouvrir aux attaques en les rendant publiques sur Internet. Après quelques moments de réflexion, nous nous sommes entendus sur le fait qu'il fallait pousser l'analyse en effectuant une analyse comparative du plus grand nombre de scénarios possibles.

Ainsi, plutôt que de se contenter d'une unique machine, nous avons décidé d'en installer plusieurs, avec des configurations différentes mais également d'apporter une dimension spatiale et temporelle à l'étude d'Hélène.

Pour obtenir des configurations différentes, il faut exposer des machines avec différentes vulnérabilités. En effet, sur une machine quelconque, nous pouvons avoir plusieurs services qui tournent en même temps. Ces services ouvrent des portes d'entrée sur la machine, que nous avons définies plus tôt comme des ports. Ces ports sont identifiés par des numéros, et sont associés à des protocoles. Par exemple, le port 80 est associé au protocole HTTP, le port 443 est associé au protocole HTTPS, etc.

L'idée ici est donc de disposer de véritables champs de poneypots, où chacun d'entre eux sera exposé à des attaques différentes, liées aux services que nous avons ouverts.

Par exemple, un premier poneypot peut faire tourner un serveur web, qui lui sera exposé à des attaques de type XSS (Cross-Site Scripting) qui permet à un attaquant d'injecter du code malveillant dans une page web. Un autre poneypot peut héberger une base de données, qui lui sera exposé à des attaques de type SQL Injection qui permet à un attaquant d'injecter du code SQL dans une requête SQL via un formulaire web permettant d'extraire des informations confidentielles par exemple, etc.

L'intérêt au travers de cette première solution pour construire notre dataset, c'est de pouvoir affirmer quels sont les services les plus efficaces pour attirer des attaques, et donc pouvoir réfléchir à monter d'autres poneypots avec ces services pour récolter encore plus de données.

Maintenant, en plus de la diversité de configuration, nous avons apporté une diversité spatiale et temporelle :

- L'aspect spatial est lié à la localisation des machines. De fait, nous avons décidé de déployer des machines à plusieurs endroits sur Internet tout en faisant en sorte qu'elles soient physiquement au même endroit. Ce procédé est réalisable grâce à des outils, tel que WireGuard, qui permettent de créer, plus ou moins, des réseaux privés virtuels (VPN) entre des machines distantes. Cela permet de faire croire à un attaquant que les machines sont physiquement à des endroits différents, qui sont donc les portes  d'entrée pour ces attaquants, alors qu'elles sont en réalité au même endroit, grâce à une redirection qui est faite par un tunnel. L'intérêt est de pouvoir évaluer les différences de comportement des attaquants en fonction de leur localisation. Par exemple, dans un premier temps l'objectif était de déployer des poneypots à Rennes ainsi qu'à Nancy avec qui nous avons réussi à obtenir un accord commun pour mener à bien cette expérience, mais nous pouvons également imaginer à l'avenir déployer des poneypots dans d'autres pays, pour évaluer les différences de comportement des attaquants en fonction de leur localisation géographique, et si des tendances communes se reflètent.

- Maintenant l'aspect temporel de l'étude est lié à la durée d'exposition des machines. Cette fois-ci l'idée est de varier les intervalles de temps auxquels les machines sont exposées, mais aussi de varier la période de l'année où elles seront exposées. Nous avons donc imaginé différents cas de figure, comme l'exposition durant une semaine, un mois et trois mois, et ce, sur différents mois. Cet aspect est celui qui nous permettra de savoir s'il y a des moments plus propices pour que des attaques aient lieu et, au contraire, des moments où les attaques se font plus rares. Si, grâce à cela, nous arrivons à déceler des paternes, nous pourrons alors réfléchir à des stratégies d'exposition plus efficaces nous permettant de collecter un plus grand nombre de données.

C'est au travers de ces trois piliers, une fois combinés, que nous allons pouvoir construire une analyse complète du problème pour englober bon nombre de scénarios d'attaques, et donc pouvoir construire un jeu de données qui nous satisfasse au mieux.

L'aspect spatial est conditionné par la mise en place du tunnel de communication. Ce point nécessite une intervention sur la plateforme Hoplab par Alexandre. En attendant, nous nous contentons d'ordinateurs physiquement présents à Rennes dans la salle serveur de l'Inria, qui tourneraient 24h sur 24 pour permettre la capture du trafic réseau en permanence, et donc nous permettre de nous concentrer sur les deux autres aspects.

== Travail réalisé

#v(5pt)

Pour ma part, un premier travail de formation dès le début du stage a été nécessaire pour comprendre les enjeux de la cybersécurité, et plus particulièrement de la détection d'intrusion. La découverte et la familiarisation avec les outils que nous allions utiliser était de-même un point clé.

Avant de déployer les poneypots, il fallait comprendre les différents rouages du projet en effectuant des tests de manière locale, et cela passe par des machines virtuelles hébergées sur ma machine. J'ai déjà eu l'occasion de créer mes propres machines virtuelles par le passé. Pour effectuer ce travail, j'ai utilisé ce qu'on appelle un hyperviseur : VirtualBox. VirtualBox facilite considérablement la tâche lorsqu'on a besoin d'une ou plusieurs machines de test pour les travaux pratiques lors d'une formation. La création d'une machine virtuelle avec son propre système d'exploitation nécessite néanmoins une phase d'installation et de configuration post-installation. 

Ce n'est pas nécessairement un problème lorsque nous avons besoin d'une machine pour une durée limitée, mais dans le cas d'un déploiement de poneypots, il est nécessaire de pouvoir automatiser cette phase afin de ne pas avoir à reproduire les mêmes opérations de configuration à chaque fois et de pouvoir déployer un grand nombre de machines en un temps record.

\
#align(left, text(12pt)[*Création de machines virtuelles*])

C'est là que _Vagrant_ entre en jeu. Le concept central autour de Vagrant est la "box" qu'il faut voir comme une machine virtuelle préconfigurée, jetable et réutilisable. Il exploite donc notre hyperviseur pour créer et gérer nos machines virtuelles selon un schéma bien précis que nous définissons dans un fichier de configuration : le Vagrantfile. @vagrant @vagrant2

Ce fichier est divisé en différentes sections, toutes ayant un rôle précis. Une première section concerne la configuration de la box. Nous pouvons y définir le système d'exploitation, la quantité de mémoire vive, la taille du disque dur, etc. Une seconde section concerne la configuration réseau, c'est-à-dire la manière dont la machine virtuelle sera connectée au réseau. Nous pouvons y définir si la machine virtuelle sera connectée à Internet, si elle sera connectée à un réseau privé, si elle sera connectée à un réseau public. Une troisième section concerne l'approvisionnement.

#pagebreak()
#align(left, text(12pt)[*Automatisation de tâches*])

L'approvisionnement est une autre tâche à laquelle j'ai dû me former rapidement. Pour cela, j'ai fait usage d'un langage d'automatisation Open Source : _Ansible_. Ansible est un outil d'automatisation qui permet de déployer des applications, de gérer des configurations et d'orchestrer des déploiements. Pour faire le lien avec mes études, il s'agit d'un outil très répandu, dans une stratégie devops, pour l'intégration et la distribution continues, et il est essentiel pour le rôle d'administrateur système lorsqu'il s'agit de gérer la maintenance de serveurs. Ansible permet de définir des tâches, en temps normal répétitives, à effectuer sur une machine distante, et de les exécuter en une seule commande, c'est en ça que consiste l'approvisionnement. Ces tâches sont décrites une à une dans un fichier appelé « playbook », et elles sont appliquées à une liste de nœuds répertoriés dans un second fichier appelé « inventaire ». @ansible \ \

#figure(
  image("figures/image-7.png", width: 100%),
  caption: [
    Ansible Architecture
    _ https://digitalvarys.com/how-to-write-an-ansible-playbook/ _
  ],
)

\ \ C'est un outil tellement efficace que ces mois de pratique m'ont offert la possibilité de faire à mon tour une formation d'introduction à Ansible au travers de travaux pratiques. Cette séance de travaux pratiques a eu lieu dans le cadre d'un évènement hebdomadaire "Papers, please" qui est un moment de partage au sein de l'équipe CIDRE pour apporter son lot de connaissances sur une thématique précise. Ces deux heures ont demandé une certaine préparation en amont pour construire un sujet de toute pièce qui était à la fois guidé par les explications que je donnais à l'oral pour définir de nouvelles notions et à la fois pratique par l'intermédiaire d'exercices à réaliser individuellement.

Lien vers le sujet du TP : https://github.com/MariusLD/tp_ansible/blob/main/guidelines/ansible101.pdf

L'intérêt d'Ansible dans notre cas est donc de pouvoir assurer un déploiement des poneypots complètement automatisé, et d'intégrer quelques variations en fonction des services que nous souhaitons exposer sur chacun des poneypots.

Pour me faire la main, je me suis donc entraîné en écrivant mes propres fichiers de configuration afin de déployer un poneypot Cowrie Low-Interaction. C'est en pratiquant que j'ai pu me rendre compte que Cowrie n'était pas ce que nous attendions spécialement d'un poneypot, mais ce fut une bonne manière d'appréhender Vagrant et Ansible.

Les semaines qui ont suivi, je me suis donc concentré sur la réflexion autour de la solution finale à mettre en place, et notamment sur la réponse à la question : comment écouter et enregistrer du trafic réseau automatiquement ? Sur ce point, Hélène et Alexandre ont pu m'aiguiller. En effet, par le passé ils ont déjà fait face à ces questions puisqu'ils ont eu l'occasion d'organiser un événement Capture The Flag (CTF). Ce genre d'évènements consiste à mettre en compétition des équipes de joueurs qui doivent résoudre des énigmes de sécurité informatique en cherchant à trouver des failles dans des systèmes. C'est dans ce contexte qu'ils ont décidé de capturer le trafic réseau produit par les participants lorsqu'ils tentaient de résoudre les énigmes.

\
#align(left, text(12pt)[*Écoute du trafic réseau*])

Ils ont alors fait usage d'un nouvel outil : _Suricata_. Suricata est un moteur de détection d'intrusion réseau et de prévention des intrusions open source. Il est capable de surveiller le trafic réseau en temps réel, et de détecter un grand nombre d'attaques. Il est également capable de capturer le trafic réseau et de le stocker dans des fichiers de logs.

Cela paraissait être l'outil idéal pour ma situation, et c'est sur cette suggestion que je me suis basé pour commencer mes recherches. Suricata a pour avantage d'être simple à installer, et de comporter tout ce dont nous avons besoin pour mener à bien ce travail, il suffit simplement de passer du temps à modifier son fichier de configuration, puis une fois que le template est prêt il n'y a plus qu'à le réutiliser d'un poneypot à l'autre grâce à Ansible. Suricata a de-même pour avantage d'être fourni avec un ensemble de règles pré-faites de détection d'intrusion qui vont nous permettre d'avoir des retours sur les procédés d'attaques qui ont été menés sur les poneypots.

Le but était d'avoir le service Suricata en route pour générer des données sur chacun des poneypots. 
Cependant, il fallait pouvoir assurer une chaîne de traitement de ces données. En effet, nous ne pouvions pas nous contenter de laisser traîner ces données brutes de trafic réseau sur les machines. Cette chaîne correspond à un cycle continu de traitement des données, pour assurer une analyse à n'importe quel moment mais surtout pour ne pas saturer les machines en termes de stockage.

\
#align(left, text(12pt)[*Gestion du stockage et génération des flows*])

Mon travail ici était de définir des scripts permettant de réaliser ce procédé. Ces scripts, rédigés en Python, sont exécutés automatiquement à différents moments prédéfinis par l'intermédiaire d'un outil appelé Crontab.
Crontab est un outil qui permet de planifier l'exécution de scripts à une fréquence donnée, idéal lorsque différents scripts doivent être exécutés régulièrement.

Nous avons fait le choix d'extraire ces données pour les stocker au Laboratoire Haute Sécurité (LHS) de Rennes par le biais d'Alexandre. Une fois ces données stockées en sécurité, nous allions pouvoir effectuer tout le traitement nécessaire pour construire notre jeu de données.

Encore une fois, il a fallu automatiser le tout et s'approprier un nouvel outil pour permettre la conversion des données brutes en flux de données. J'aurais pu me contenter d'un outil comme CICFlowMeter comme mentionné précédemment, mais il s'agit d'un software qui requiert un certain nombre de ressources pour fonctionner, ce que je n'ai pas jugé judicieux pour commencer. À la place, j'ai fait usage d'_Argus_. _« L'architecture Argus est conçue pour prendre en charge l'audit de réseau à petite et très grande échelle. Les données en temps réel fournissent beaucoup d'informations, qui peuvent être stockées dans des fichiers pour être traitées ultérieurement, ou les programmes clients peuvent être assemblés pour fournir des flux de données réseau en temps réel pour une sensibilisation simple au réseau, une visibilité distribuée à grande échelle, voire une cyberdéfense active »_. @argus

Dans le processus, Argus est donc capable de récupérer nos données brutes, au format pcap, et de les convertir en flux de données, au format argus. Il est ensuite possible de sélectionner les features qui nous intéressent le plus grâce à un fichier de configuration à établir, pour au final nous retrouver avec un fichier au format csv contenant tous les flux de données que nous souhaitons
(l'avantage, en utilisant Argus plutôt que CICFlowMeter, est la rapidité d'exécution du processus. En effet, une des limites rencontrées avec CICFlowMeter est le fait que toutes les données brutes sont chargées en mémoire avant leur traitement. Pour des périodes de capture courte cette solution est viable, mais pour des mois de capture nous nous retrouvons très rapidement avec des fichiers très lourds de capture, qui requièrent une quantité importante de ressources à allouer).

Avoir écrit un tel script nous permet de sélectionner précisément des features et d'ignorer des données qui pourraient être sensibles, comme les adresses IP, ce qui pourrait permettre à l'avenir d'obtenir des accords pour la récupération de données (de la part du CHU de Rennes entre autres), et indirectement des accords de publication. Ici, les principales features qui nous intéressent sont les ports, les protocoles, les durées des connexions, les nombres de paquets envoyés par les attaquants, les timestamps (l'horodatage) et bien d'autres, rien qui ne puisse révéler des informations sensibles.

\
#align(left, text(12pt)[*La fause vie d'un poneypot*])

Après avoir paramétré cette capture, j'ai dû travailler sur une autre notion autour des poneypots : la question de la fausse vie. Comme expliqué auparavant, les honeypots sont des machines qui servent d'appât et qui doivent tout faire pour imiter au mieux le comportement d'une vraie cible d'attaques.
La fausse vie c'est un concept qui a double effet dans notre cas :

- Faire croire à un attaquant qu'il est face à une vraie cible passe aussi par la simulation d'un comportement humain sur le honeypot en incluant des activités telles que :

    - La gestion de mails : en effet, il est possible de faire tourner un serveur mail sur un honeypot, et de simuler des échanges de mails avec d'autres machines ainsi que le téléchargement de pièces jointes.

    - La navigation sur Internet : que ce soit en effectuant des recherches, en naviguant sur des sites web, en interagissant avec des services en ligne comme le streaming ou bien en visitant des médias sociaux.

    - La gestion de fichiers : en créant des fichiers, en les modifiant, en les supprimant, en les déplaçant, etc. Ce genre d'activité système contribue à copier le comportement humain, ou du moins ce que nous pourrions également faire sur notre propre machine.

Il est alors possible de définir différents profils d'utilisateurs qui auront des routines différentes (un profil travailleur aura plus tendance à consulter ses mails qu'une personne lambda) tout en gardant de la cohérence que ce soit dans les plages horaires où ces activités se produisent, ou bien la vitesse à laquelle les actions s'enchaînent, il faut reproduire un comportement humain le plus fidèlement possible.

- L'autre effet de la fausse vie concerne la phase d'analyse des données que mènera Hélène lors de ses expériences. Il est vrai, la détection d'intrusion repose bien évidemment sur la distinction des activités malveillantes et bénignes. En incorporant nous même des activités bénignes, nous augmentons la difficulté de la tâche de distinction, ce qui a pour effet de crédibiliser le modèle de Machine Learning si les résultats de cette phase de séparation des types d'activités s'avèrent concluants.

La suite de mon travail a porté sur la protection des poneypots. Je l'ai stipulé précédemment, mais le fait de monter des poneypots High-Interaction implique de créer un système vulnérable et surtout exploitable. Or des contraintes éthiques et morales nous empêchent de laisser des machines hors de notre contrôle, surtout si elles sont à l'intérieur du réseau d'Hoplab, au-delà du firewall. Le risque derrière ça serait que nos propres poneypots soient utilisés dans les réseaux de botnets pour attaquer d'autres machines, ou bien que du contenu illégal et indésirable soit stocké à notre insu.

\
#align(left, text(12pt)[*Monitoring et statistiques*])

Il a donc fallu une nouvelle fois créer des scripts qui ont cette fois-ci pour rôle de détecter des tentatives réussies d'accès aux poneypots, et d'alerter l'équipe par mail qu'une faille de sécurité a eu lieu. Cela nous permet de réagir rapidement en cas de compromission, et de surveiller en détail l'activité qui se produit sur la machine. Les intentions des cybercriminels nous intéressent, mais dès lors qu'une activité nous paraît compromettante, il est nécessaire de couper instantanément le poneypot pour contenir la menace. La sécurité passe de-même par la prévention, en définissant des mots de passe robustes pour obtenir l'accès aux services exposés, ou bien par le chiffrement/dissimulation de toutes les informations sensibles qui pourraient se trouver sur ces poneypots.

Une autre partie de mon travail consistait à récupérer les flows de données générés par les poneypots, et d'effectuer des statistiques sur ceux-ci. Ces statistiques portaient sur plusieurs aspects :

- Le nombre de tentatives d'intrusions, en fonction de différentes timelines, que ce soit par heure, par jour, par semaine, par mois ou par an. C'est une manière d'effectuer une première analyse de l'aspect temporel de l'étude.

- Les utilisateurs les plus visés (puisqu'une unique machine peut comporter un grand nombre d'utilisateurs, les attaquants cherchent généralement ceux qui existent afin de tenter une attaque par force brute via le protocole SSH, qui vise à tenter des milliers de combinaisons possibles en associant un mot de passe à un utilisateur) et les ports associés à ces utilisateurs lors de ces attaques.

- Les protocoles réseaux ainsi que les ports les plus visés. C'est une manière d'effectuer une première analyse des services les plus efficaces pour attirer des attaques, et donc de pouvoir réfléchir à la création de nouveaux poneypots avec ces services ouverts.

L'ultime tâche que j'ai eu à effectuer était le suivi des poneypots. Il s'agissait d'une phase où il fallait que je m'assure que les différents fichiers de configuration comportaient les bonnes options selon les résultats que nous obtenions, il fallait que je sois certain que le cycle de récupération des données marche sans accroc et sans aucune perte, et il fallait que je m'assure que les poneypots soient toujours en ligne, accessibles et sous contrôle.

Finalement, j'ai eu la chance de pouvoir présenter ce travail effectué lors d'un évènement qui s'est déroulé à CentraleSupélec  : « La recherche sur le grill » qui était un moment convivial, entre les différentes équipes de recherche présentes, de manière à partager ses travaux. Il s'agissait surtout de vulgariser son travail pour être compris de tous, puisque les équipes n'avaient pas nécessairement le même domaine d'expertise. Les retours étaient plutôt positifs, à la hauteur des répétitions effectuées au préalable avec Hélène, et c'était un bon moyen de partager le projet que je mène autrement qu'au travers de ce rapport. La passion pour la recherche était palpable et les sujets étaient très enrichissants.

Lien vers le support de la présentation : https://docs.google.com/presentation/d/1Op_Kp-zGRr0UvfFNWfrlUT5l-RyotsQ60RtatzeYSJM/edit?usp=drive_link

#pagebreak()

== Résultats développés

#v(5pt)

Cette première phase expérimentale s'est conclue avec une extraction de toutes les données récoltées, et ce, le dernier jour de mon stage. Il s'agit d'un premier jeu de données qui a été construit à partir des deux machines physiques que nous avons déployées à Rennes. La première que nous allons appeler poneypot1 a été lancée le 17/08/23 et la seconde, poneypot2, a été lancée un mois plus tard. Ce jeu de données est donc composé de deux fichiers de flows générés, un pour chaque poneypot, ainsi que des fichiers repertoriant toutes les alertes de sécurité décelées par Suricata. Ces alertes nous ont permis de visualiser les vulnérabilités principalement visées par les attaquants. C'est ce même jeu de données qui va contribuer à la rédaction et la publication d'un papier scientifique d'Hélène prévu pour le mois de septembre. L'activité enregistrée par ces poneypots était principalement du scan de ports et des tentatives de connexion.

De mon côté, j'ai pu lancer mes quelques scripts permettant de faire des statistiques. Le premier consiste à lister les ports et les utilisateurs les plus visés lors de ces tentatives d'attaques en fonction du protocol utilisé. Le second quant à lui est le script permettant de lister les tentatives d'attaques en fonction de différentes timelines.

Nous avons donc pu prendre du recul sur cette première phase expérimentale, afin de lister les potentielles erreurs que nous ne souhaitions pas répliquer, mais aussi les points positifs que nous souhaitions conserver pour la suite.

Ce dernier jour de stage n'a pas seulement marqué la fin de cette expérience mais également le début de la prochaine : la mise en place de quatre poneypots avec des rôles bien définis. En effet, ce dernier mois de stage aura été marqué par plusieurs heures de travail en collaboration avec Alexandre pour finir de mettre en place l'architecture souhaitée avec le tunnel de communication. Il était désormais temps de mettre en place la solution "finale" dans laquelle nous allions pouvoir déployer nos poneypots, mais surtout en ouvrir un certain nombre afin de laisser des individus s'introduire sur les machines, les laisser opérer un minimum pour capturer de l'activité, puis les clôturer.
Après mon départ, il reste donc derrière moi : un poneypot qui dispose du service SSH d'ouvert avec un mot de passe fort, un autre dont le service SSH est ouvert avec un mot de passe faible, un poneypot avec une base de données MongoDB d'ouverte à accès faible et un dernier qui dispose d'une base de données MongoDB également à accès faible mais aussi du service SSH d'ouvert avec un mot de passe faible. L'idée derrière cette configuration est de comparer les résultats obtenus et constater le niveau de compromission des machines en fonction des services ouverts.

Pierre-Victor a pu constater l'avancée de ce travail et a voulu y contribuer en ajoutant un de ses honeypots Cowrie à notre architecture. Ce honeypot, lui aussi à SSH faible, a pour particularité d'accepter l'escalation de privilèges. C'est-à-dire que si un attaquant parvient à s'introduire sur la machine, il pourra alors obtenir les droits d'administrateur et donc avoir un accès complet à la machine. Les autres honeypots ne permettent pas cela par souci de sécurité. Celui-ci nous permettra donc de constater si les attaquants sont capables d'exploiter cette vulnérabilité, et si ils cherchent à devenir administrateur de la machine une fois qu'ils y ont accès.

#pagebreak()

#set page(header: [
    #set text(11pt) 
    #set align(left)
    _Déploiement d'Honeypots_
    #h(1fr)
    *_Evaluation_*
    ])

= Évaluation

#v(10pt)

== Interprétation des résultats

#v(5pt)

Pour ce qui est du type d'activité enregistré, nous nous doutions pertinemment du résultat puisqu'il s'agissait surtout de scan de ports et de tentatives de connexion. Le fait d'exposer un unique service et de le renforcer le plus possible limite, sans aucun doute, le champ des possibles pour les attaquants. Nous avons pu le constater notamment grâce à la visualisation des paquets réseaux, la plupart du temps une nouvelle IP détectait notre machine, puis tentait de s'introduire. De notre côté, nous n'avons jamais constaté d'intrusion sur la machine grâce au monitoring mis en place, nous en avons donc conclu que toutes les tentatives perçues étaient des échecs.

Pour ce qui est de l'interprétation des résultats, nous pouvons prendre en considération les statistiques établies. Premièrement nous pouvons visualiser le graphique présentant les ports les plus visés en fonction des différents protocoles utilisés. Nous avons fait le choix de distinguer deux protocoles du reste : TCP (Transmission Control Protocol) et UDP (User Datagram Protocol) qui sont deux des protocoles les plus couramment utilisés et sont essentiels pour le transfert de données sur les réseaux. Dans le cas de poneypot1 (ou Poney41 ici) nous avons obtenu les graphiques suivants :

#figure(
  image("figures/Figure_3.png", width: 150%),
  caption: [
    Ports les plus visés pour le protocole TCP sur poneypot1
  ],
)

#pagebreak()

Nous pouvons constater que le service SSH, par le biais du port 22, était majoritairement visé à hauteur de 74.2% des "attaques" ce qui est logique puisque c'est le seul service que nous avons ouvert sur cette machine, une fois cette ouverture détectée les attaquants ont réitéré leurs attaques à maintes reprises pour tenter de forcer l'accès. \ Nous pouvons constater que le port 23 était visé à hauteur de 7.4%, ce qui est logique puisque c'est le port par défaut du service Telnet, qui est un service similaire au SSH mais qui est moins sécurisé et donc de moins en moins utilisé. \ Le port 80 a concentré 5.3% des attaques, ce qui est également intuitif puisque c'est le port par défaut du service HTTP, qui est un service qui permet de faire tourner un serveur web. \ Le port 443 était aussi une cible de choix à hauteur de 11% des attaques capturées, ce qui fait sens puisque nous savons que c'est le port par défaut du service HTTPS, qui est un service qui permet de faire tourner un serveur web sécurisé. \ Nous pouvons également constater que le port 8080 était visé à hauteur de 1%, ce qui est cohérent puisque c'est le port par défaut du service HTTP alternatif, qui est un service qui permet de faire tourner un serveur web sur un port alternatif. Finalement nous pouvons en déduire que, malgré une forte domination du port 22 notamment dûe à l'ouverture du service SSH, les attaquants ont tout de même tenté de s'introduire sur d'autres ports en scannant ceux sur lesquels des services ont pu être mal sécurisés.

#figure(
  image("figures/Figure_4.png", width: 150%),
  caption: [
    Ports les plus visés pour le protocole UDP sur poneypot1
  ],
)

#pagebreak()

Le protocole UDP est utilisé dans la résolution des noms de domaine en adresses IP sur Internet (DNS) c'est-à-dire que l'adresse IPv4 8.8.8.8 sera traduite, grâce au DNS, par "Google". Ce protocole est très régulièrement appelé pour savoir avec qui nous communiquons sur le réseau, et nous le ressentons d'ailleurs dans le graphique ci-dessus puisqu'il s'agissait de 77.4% des communications établies via UDP. Nous pouvons également constater que le port 1091 était visé à hauteur de 6.6% des conversations enregistrées, puisqu'il s'agit du port par défaut du service FF-FastTrack, qui est un service qui permet de faire du partage de fichiers en P2P. \ Finalement nous pouvons en déduire que les attaquants ont tenté de s'introduire sur le port 53 en scannant ce dernier, mais que les autres ports n'ont pas été visés. Enfin, le Multicast DNS (mDNS) est grandement representé puisqu'il s'agit d'un protocole utilisé pour résoudre les noms de domaine mais cette fois-ci à une échelle locale, sans avoir besoin d'un serveur DNS centralisé.
Ce graphique au sujet du protocole UDP ne nous en apprend pas tellement tant sur le comportement des attaquants dans un contexte où le poneypot est fermé. UDP est un protocole de couche transport qui ne garantit pas la livraison des paquets ni l'ordre dans lequel ils sont reçus. \ Ainsi l'intérêt du protocole, pour de la détection d'intrusion, se ferait surtout ressentir dans l'étude de divers types d'attaques, comme les attaques par déni de service (DDoS), les attaques de réflexion, et les attaques de fuzzing. Les attaques de réflexion vont par exemple viser à exploiter des serveurs UDP mal configurés pour amplifier un trafic, tandis que le fuzzing lui va viser à envoyer des paquets UDP mal formés pour tenter de faire crasher un service.

#figure(
  image("figures/Figure_5.png", width: 150%),
  caption: [
    Ports les plus visés sur poneypot1
  ],
)

#pagebreak()

Voici le récapitulatif de tous les ports les plus visés sans prendre en compte un protocole spécifique. Nous pouvons constater que, outre les ports détaillés plus tôt, la plupart des ports qui sont régulièrement revenus (au-delà des 1000 occurrences) sont des ports très élevés. Il s'agit de ports qui sont généralement utilisés pour des services personnalisés, principalement des bases de données, et qui sont donc plus difficiles à identifier mais qui offrent des données plus intéressantes. Il est possible de changer les ports par défaut pour un grand nombre de services, le but pour les attaquants est donc de les trouver en testant des ports alternatifs connus ou par tâtonnement.

Maintenant nous pouvons jeter un coup d'œil à la quantité d'intéractions qu'il y a eu avec ces poneypots. Pour ce qui est de poneypot1 nous avons pu construire ces graphiques :

#figure(
  image("figures/timeline_Day_08-19-2023_sam.png", width: 75%),
  caption: [
    Activité enregistrée sur poneypot1 le 19/08/2023
  ],
)

#figure(
  image("figures/timeline_Week_28.png", width: 75%),
  caption: [
    Activité enregistrée sur poneypot1 la semaine du 11/07/2023 au 17/07/2023
  ],
)

#figure(
  image("figures/timeline_Month_2023-07.png", width: 75%),
  caption: [
    Activité enregistrée (coupée) sur poneypot2 pour le mois de juillet 2023 
  ],
)

#figure(
  image("figures/timeline_Month_2023-08.png", width: 75%),
  caption: [
    Activité enregistrée (coupée) sur poneypot1 pour le mois d'août 2023
  ],
)

Nous pouvons constater que l'activité réseau étendue sur 2 mois est plutôt régulière, malgré quelques outliers, ce qui peut nous laisser penser qu'il n'y a pas nécessairement de moment plus propice qu'un autre pour lancer une expérience avec ces poneypots, mais il faudrait l'évaluer sur une plus longue durée ou à une autre période de l'année pour pouvoir se permettre de généraliser ce constat.

#pagebreak()

Pour finir sur la démonstration des résultats, voici un échantillon des données récoltées pour construire le jeu de données :
#figure(
  image("figures/C2.png", width: 140%),
  caption: [
    Fichier pcap issu du Suricata de poneypot1
  ],
) \

Le fichier pcap représenté sur la Figure 9 est celui qui est lu par l'intermédiaire de Wireshark (un analyseur de paquets) et qui retrace tous les paquets ayant circulés sur le réseau. Ici, il s'agit d'une tentative d'intrusion au travers de SSH provenant d'une certaine adresse IP. Tous ces paquets échangés correspondent à une conversation débutée à 21:38:54 le 9 août 2023 et faite entre l'individu et notre poneypot. Cette conversation contient notamment une phase d'échange de clés, relative au protocole SSH. Ce sont de réelles données brutes, compliquées à analyser lorsque nous avons un fichier pcap de plusieurs gigaoctets. 

\

#figure(
  image("figures/C1.png", width: 110%),
  caption: [
    Flow généré avec Argus
  ],
)

Après passage de ces données au travers de mon script de conversion en flow, nous obtenons le résultat présenté sur la Figure 10. L'échange de paquets mentionnés juste avant est maintenant encadré en rouge dans le fichier csv. Nous pouvons clairement voir que l'information est condensée, tout en préservant les champs qui nous importent, à savoir : l'horodatage, le temps d'échange, le protocole utilisé, l'adresse IP source, le port source, la direction du flow, l'adresse IP destination, le port destination, l'état de la connexion, le type de service source, le type de service de destination, le nombre de paquets, le nombre d'octets, le nombre d'octets source. \
Nous pouvons donc comprendre que sur cette capture sont représentés de multiples tentatives d'intrusion effectuées par la même IP repérée dans le fichier pcap, et à des intervalles tellement proches que cela peut nous laisser penser qu'il s'agit d'un bot chargé de "bruteforcer" des mots de passe sur des machines.

Pour ce qui est du fichier contenant les alertes capturées par Suricata, nous en retrouvons un grand nombre, pour la plupart sans grand intérêt (Suricata nous signale simplement qu'une IP détectée et connue comme étant malveillante a tenté de se connecter), mais d'autres ont pu montrer des vulnérabilités exploitées, issues de CVE (Common Vulnerabilities and Exposures) liées à différents services/softwares ou pièces hardwares, mais sans succès. L'intérêt de ces alertes était moindre.

\

== Retour d'expérience et conclusion

#v(5pt)

Je peux sincèrement affirmer que cette période de trois mois de stage était réellement enrichissante en tout point. J'ai eu la chance d'avoir été correctement encadré et régulièrement suivi, via des réunions hebdomadaires, pour sentir que mon travail était valorisé. J'ai pu découvrir un domaine qui m'était inconnu dans la pratique, et qui m'a permis de me rendre compte de l'importance de la cybersécurité, et plus particulièrement de la détection d'intrusion dans un contexte de recherche, ainsi que tous les enjeux qui en découlent. J'ai su être à l'écoute de tous les conseils qu'on a pu m'apporter, de part l'expertise de chacun, et monter en compétence rapidement sur les technos que je découvrais. L'environnement de travail était sain, notamment par son ambiance agréable, grâce aux doctorants et permanents toujours prêts à échanger autour de différents sujets.

J'ai très rapidement dû me convertir en peer worker avec Alexandre pour établir rapidement des stratégies de développement d'architecture autour des poneypots, de sorte à ce que cette dernière convienne aux besoins de chacun mais aussi pour qu'elle puisse être réutilisée aisément lors de futurs évènements CTF. Je sors plus riche, après ces quelques mois de stage, puisqu'il m'a apporté un grand nombre de connaissances, notamment dû à son statut d'ingénieur au service de la recherche et de son expertise liée à des projets plus ou moins similaires qu'il a pu mener. Cette complémentarité nous a également permis d'avancer plus rapidement en combinant généralement mon travail et son travail, effectués en amont.

J'ai aussi appris à aimer la remise en question perpétuelle de mon travail, qui était nécessaire lorsque nous rencontrions de nouvelles limites au fur et à mesure. Je devais me rendre à l'évidence qu'il était impossible de tout prévoir dès le début, et c'est aussi une part du travail de recherche, au point de devoir changer l'expérience finale lors des dernières semaines.

Le travail autour de l'IA était moins présent lors de ce stage de mon côté, il s'agissait surtout de la tâche importante d'Hélène, elle m'a néanmoins sollicité pour une tâche minime où l'idée était de préparer un script lui permettant de comparer son modèle, en termes de performances, à des modèles connus de la littérature.

Hélène m'aura d'ailleurs également beaucoup apporté, encore une fois, par sa disponibilité et ses conseils, mais aussi en me poussant à participer à différents évènements : « La recherche sur le grill » ou « Papers, please » auxquels je n'aurais probablement pas ambitionné de m'inclure au premier abord, mais qui m'ont finalement intéressés et pour lesquels je me suis investis.

#pagebreak()

#set page(header: [
    #set text(11pt) 
    #set align(left)
    _Déploiement d'Honeypots_
    #h(1fr)
    *_Bibliographie_*
    ])

#v(10pt)

#bibliography("bibliography.yml")

#pagebreak()

#set page(header: [
    #set text(11pt) 
    #set align(left)
    _Déploiement d'Honeypots_
    #h(1fr)
    *_Annexe_*
    ])

= Annexe

#v(10pt)

== Glossaire

#v(5pt)

== Grille RSE

Étant donné que mon stage s'est inscrit dans le cadre d'un laboratoire de recherche et non celui d'une entreprise, il était nécessaire de redéfinir certaines colonnes de la grille pour correspondre aux objectifs dans ce contexte, notamment celles autour des affaires et les questions relatives aux consommateurs/clients puisqu'il n'y a pas de fond lucratif derrière. Les références suivantes ont permis d'appuyer certains points :
- https://www.centralesupelec.fr/fr/presentation
- http://www.rennes.centralesupelec.fr/environment
- https://www.centralesupelec.fr/sites/default/files/mcf_cdi_cidre_irisa.pdf
- https://www.letudiant.fr/classements/classement-des-ecoles-d-ingenieurs.html
- https://www.mondedesgrandesecoles.fr/tous-les-voyants-sont-au-vert-pour-les-ingenieurs-de-centralesupelec-en-2021/
- https://www.centralesupelec.fr/fr/centralesupelec-ndeg2-du-classement-du-figaro-etudiant-des-ecoles-dingenieurs-2023

#v(5pt)

#table(columns: (25%, 25%, 50%), rows: (auto,auto,auto,auto,auto,auto,auto),  stroke : black, inset : 8pt,fill : (col,row)=> if row == 0 {gray} else {white},
    text(12pt)[*Catégorisation de l'axe RSE*], 
    text(12pt)[*Intitulé de l'action mise en oeuvre au sein de l'organisation*],
    text(12pt)[*Objectif poursuivi par l'organisation*], 

    [_Gouvernance_], [
        - Respect de l'éthique dans le milieu de la recherche
        - Conformité aux réglementations de la recherche, plus précisément dans le domaine de la cybersécurité
        - Indépendance de la gouvernance
    ], [
        - Un doctorant qui rejoint une des équipes de recherche à CentraleSupélec se voit imposé de suivre une formation à l'éthique pour les futurs travaux qu'il va proposer, cela inclut la protection des droits des participants humains, l'intégrité de la recherche, et la conformité aux normes. Cela passe également par la transparence dans les processus de recherche, y compris la manière dont les données sont collectées, analysées et interprétées
        - L'équipe s'engage notamment à être conforme à la norme internationale ISO 27001 qui s'axe sur la sécurité de l'information, d'autant plus autour de la responsabilité lors de fuites de données confidentielles (comme dans le cadre des honeypots déployés et la divulgation des données récoltées)
        - Malgré le fait que l'équipe CIDRE soit rattachée à l'IRISA (Institut de Recherche en Informatique et Systèmes Aléatoires) elle reste néanmoins associée à CentraleSupélec et est donc autogérée sur les décisions à prendre. Elle est composée de membres du corps professoral qui exercent un rôle décisionnaire au sein de l'équipe (pour des questions telles que la répartition des responsabilités, la coordination avec d'autres unités de recherche ou tout simplement des changements en interne comme le changement de nom de l'équipe qui devrait s'officialiser d'ici peu) dont la direction est assurée par Valérie Viet Triem Tong, mais aussi de membres externes faisant partie de la DGA-MI, d'ingénieurs de recherche, et de doctorants.
    ],
    [_Relations et conditions de travail_], [
        - Formation à la prévention et sécurité au travail. Politiques de ressources humaines pour les chercheurs, enseignants et personnels administratifs
        - Soutien à la diversité et à l'inclusion dans le milieu de la recherche et de l'enseignement
        - Volonté de créer un environnement de travail agréable, en proposant des espaces de travail et de détente de qualité, et volonté de préserver de bonnes relations au sein des équipes
    ], [
        - L'arrivée dans ce milieu s'accompagne d'une formation obligatoire organisée par l'IRISA, sensibilisant aux risques liés à la sécurité informatique et aux bonnes pratiques à adopter. Cette formation est obligatoire même pour des stagiaires présents sur une courte durée.
        - L'école a mis en place une politique de ressources humaines pour les chercheurs, enseignants et personnels administratifs, qui vise à améliorer les conditions de travail et à favoriser l'épanouissement professionnel et personnel de chacun. Le milieu de la recherche est de-même, de manière générale, très ouvert et CentraleSupélec s'engage à soutenir cette inclusion. Les équipes de recherche sont d'ailleurs composées de personnes de divers horizons, qui sont évidemment considérées pour ce qu'elles sont et leur qualité de travail.
        - L'école est constituée de différents bâtiments, de différents étages où chaque équipe de recherche dispose de son propre espace de travail. Ces espaces sont généralement composés de bureaux individuels, de salles de réunion, de salles de pause, permettant d'allier travail et échanges. Les relations dans les équipes sont très bonnes, il est courant d'organiser des sorties en dehors du cadre professionnel pour renforcer ces liens, de profiter des tables de tennis de table, de billard, de babyfoot mises à disposition, ou bien de profiter des infrastructures sportives du campus.
    ],
    [_Qualité de la recherche et de l'enseignement_], [
        - À la pointe de la recherche et de l'enseignement
        - Une offre complète de formations d'excellence
    ], [
        - Dans le classement des écoles d'ingénieurs françaises, CentraleSupélec se place à la troisième place selon le classement publié en 2023 par le Figaro Étudiant, et se positionne également comme étant le premier établissement français du classement international Employabilité de Times Higher Education (THE) témoignant ainsi de la qualité de l'école et de ses équipes de recherche.
        - CentraleSupélec et ses équipes de recherche forment une institution de références mondiale pour ses 4 socles d'activité, à savoir, la formation d'ingénieurs généralistes de haut niveau, la recherche en sciences de l'ingénieur et des systèmes, la formation de docteurs, la formation continue, et la formation Mastère Spécialisé
    ], 
    [_Engagement avec la communauté académique_], [
        - Collaboration avec d'autres institutions de recherche
        - Participation à des initiatives de recherche internationales Sophia
        - Partage des connaissances et des résultats de la recherche Séminaire Recherche sur le Grill, Papers please
        - Soutien à la diffusion de la recherche au sein de la communauté universitaire
        - Promotion de la recherche responsable
    ], [
        - L'école (et ses différentes parties) est membre du CDGGEB (Conférence des Directeurs des Grandes Ecoles de Bretagne) et a noué de nombreux partenariats de formation et de recherche avec des institutions locales : Université Rennes 1, IGR Rennes Management Institute, ENS Rennes, ENSAI, IMT Atlantique, INSA Rennes. Les équipes de recherche font partie de laboratoires mixtes partagés avec les établissements précités, l'IETR (Institut d'Electronique et des Technologies Numériques),l'IRISA et avec les organismes de recherche CNRS (Centre National d'Etudes Scientifiques). Recherche) et INRIA . Ils sont également impliqués dans les deux écoles de recherche supérieures CyberSchool et DigiSport.
        - L'école voit grand et s'implique à l'internationale, que ce soit en proposant aux doctorants de partir à l'étranger pour effectuer des recherches dans un laboratoire partenaire (en Allemagne par exemple) et en proposant de participer à des conférences internationales (au Japon, en Chine, etc.) ou bien en offrant la possibilité de participer à des évènements en France comme l'école d'été Cyber à Sophia Antipolis. Son implication à l'internationale se ressent également au niveau de son classement puisqu'elle se place parmi les 200 meilleures universités mondiales.
        - Le partage est au cœur des ambitions de l'école et des équipes de recherche. Nous pouvons retrouver des posters présentant des travaux de thèse ou de recherche effectués, et des phases de démonstration auprès des étudiants pour les initier au monde de la recherche. Ce partage de connaissances se ressent également au travers d'évènements comme « La recherche sur le grill » ou les rendez-vous hebdomadaires « Papers, please ».
    ], 
    [_Environnement_], [
        - Attribution du label DD en janvier 2023 pour une durée de 4 ans
        - Sensibilisation par le CNRS (Centre National de la Recherche Scientifique) aux enjeux environnementaux
        - Volonté de préserver la biodiversité sur le campus de CentraleSupélec Rennes
    ], [
        - Ambition stratégique visant à porter les transitions énergétiques et écologiques, la préservation de l'environnement, le développement de constructions, infrastructures et mobilités durables
        - Prise de parole sur l'éco responsabilité, comment concilier numérique et environnement
        - Une des particularités du campus est d'être situé dans un parc boisé de plusieurs hectares très bien entretenus, démontrant ainsi la volonté de préserver un espace vert de qualité
    ],
    [_Engagement sociétal_], [
        - Attribution du label RS en janvier 2023 pour une durée de 4 ans
    ], [
        - Ambition stratégique visant à améliorer la santé, et plus généralement la qualité de vie, pour tous, afin notamment de réduire les fractures sociales, tout en contribuant aux équilibres du monde, notamment en préservant les souverainetés européennes, industrielles, alimentaires et numériques (au travers de partenariats bilatéraux et de projets coopératifs nationaux/internationaux)
    ])