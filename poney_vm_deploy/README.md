# Get ANSIBLE vairables
ansible rainbowsur-rba -m ansible.builtin.setup -i .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory > rainbowsur-rba.txt

# Set UP Wireguard interfaces
ansible-playbook -i .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory --extra-vars "target=*gw-rba wg_action=up" --ssh-common-args='-o StrictHostKeyChecking=no' wg-quick.yml 

# Set DOWN Wireguard interfaces
ansible-playbook -i .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory --extra-vars "target=*gw-rba wg_action=down" --ssh-common-args='-o StrictHostKeyChecking=no' wg-quick.yml 

# Restart SURICATA service
ansible-playbook -i .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory --extra-vars "target=*sur-*" --ssh-common-args='-o StrictHostKeyChecking=no' suricata-restart.yml 

