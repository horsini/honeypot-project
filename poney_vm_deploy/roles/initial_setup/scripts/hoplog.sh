while true; do 
  inotifywait -r -e modify,create,delete /var/log
  rsync -avz --exclude "hoplab" /var/log/ /var/log/hoplab
done
