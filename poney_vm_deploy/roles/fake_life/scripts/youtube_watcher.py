import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from pyvirtualdisplay import Display
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import os
import re
import random

# Config PyVirtualDisplay
# display = Display(visible=0, size=(1920, 1080))
# display.start()
# Config Selenium
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--window-size=1920,1080")
chrome_options.add_argument("--disable-extensions")
chrome_options.add_argument("--proxy-server='direct://'")
chrome_options.add_argument("--proxy-bypass-list=*")
chrome_options.add_argument("--start-maximized")
#chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--ignore-certificate-errors')
chromedriver_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "chromedriver")
service = Service(chromedriver_path)
driver = webdriver.Chrome(service=service, options=chrome_options)
driver.maximize_window()

youtube_url = "https://www.youtube.com"
videos_count = 2
timeout = 10
wait = WebDriverWait(driver, timeout)

def several_ads(driver):
    print("Vérification du nombre de publicités...")
    driver.implicitly_wait(10)
    try:
        ad_div = driver.find_element(By.CSS_SELECTOR, 'ytp-ad-text')
        cleaned_text = ad_div.text.strip().replace('\n', ' ')
        print(cleaned_text)
        if "Annonce 1 sur" in cleaned_text:
            match = re.search(r'sur (\d+)', cleaned_text)
            if match:
                print("MATCH!")
                ads_number = match.group(1)
                return (True, int(ads_number))
        return (False, 0)
    except:
        return (False, 0)

def ad_in_progress(driver, field):
    try:
        ad_div = driver.find_element(By.XPATH, field)
        cleaned_text = ad_div.text.strip().replace('\n', ' ')
        if "La vidéo sera lue après cette annonce" not in cleaned_text:
            print("Publicité terminée!")
            return False
        else:
            print("Publicité en cours...")
            return True
    except:
        print("Publicité terminée!")
        return False

# Attendre que la publicité non ignorables se termine
def unskippable_ad(driver):
    field = '//div[contains(@class, "ytp-ad-text") and contains(@class, "ytp-ad-preview-text")]'
    try:
        print("En train de déterminer si la publicité est skippable...")
        ad_div = driver.find_element(By.XPATH, field)
        cleaned_text = ad_div.text.strip().replace('\n', ' ')
        if ("La vidéo sera lue après cette annonce" or "La vidéo sera lancée après les annonces") in cleaned_text:
            print("Attente de la fin de la publicité impossible à skip...")
            while ad_in_progress(driver, field):
                time.sleep(1)
                pass
            return True
        else:
            print("Publicité skippable!")
            return False
    except:
        print("Il est possible d'ignorer cette publicité")
        return False
    
def manage_ads(driver):
    # Attendre que le bouton "Skip Ad" soit cliquable (maximum 30 secondes)
    for _ in range(30):
        try:
            # Rechercher le bouton "Skip Ad" par sa classe spécifique
            skip_ad_button = driver.find_element(By.XPATH, '//button[contains(@class, "ytp-ad-skip-button") and contains(@class, "ytp-button")]')
            # Cliquer sur le bouton "Skip Ad" pour ignorer l'annonce
            skip_ad_button.click()
            print("Publicité ignorée!")
            break
        except:
            try:
                # Vérifier si l'annonce est toujours présente
                ad_div = driver.find_element(By.CLASS_NAME, 'ytp-ad-text')
                print("Tentative échouée")
            except NoSuchElementException:
                break
            pass
        # Attendre 1 seconde avant de réessayer
        time.sleep(1)

def ad_handler(driver):
    print("Publicité détectée!")
    multiple_ads_present, ads_number = several_ads(driver)
    print("Plusieurs pubs ? " + str(multiple_ads_present) + " Nombre de pubs: " + str(ads_number))
    if not multiple_ads_present:
        if not unskippable_ad(driver):
            manage_ads(driver)
    else:
        for _ in range(ads_number):
            if not unskippable_ad(driver):
                manage_ads(driver)
                break
                    

def video_hover(driver):
    video_player = driver.find_element(By.CSS_SELECTOR, '.html5-video-player')
    video_player.click()
    video_player.click()
    # hover = ActionChains(driver).move_to_element(video_player)
    # hover.perform()

def get_video_duration(driver):
    video_hover(driver)
    duration_element = driver.find_element(By.XPATH, '//span[@class="ytp-time-duration"]')
    return duration_element.text

def watch_videos(video_urls, driver):
    print(f"Les vidéos à regarder sont: {video_urls}")
    # Ouvrir les deux premières vidéos dans de nouveaux onglets
    for video_url in video_urls:
        # Ouvrir la vidéo dans un nouvel onglet en utilisant le lien vidéo
        driver.execute_script(f"window.open('{video_url}', '_blank');")
        # Passer à l'onglet récemment ouvert
        driver.switch_to.window(driver.window_handles[-1])
        # Vérifier si une publicité est présente
        ad_present = False
        driver.implicitly_wait(10)
        try:
            # Vérifier la présence de la classe "ytp-ad-text"
            ad_div = driver.find_element(By.CLASS_NAME, 'ytp-ad-text')
            ad_present = True
        except:
            pass

        if ad_present:
            ad_handler(driver)
        else:
            print("Aucune publicité détectée!")
        duration_text = get_video_duration(driver)
        while duration_text == '':
            print("Durée de la vidéo non détectée, réessayer...")
            duration_text = get_video_duration(driver)
        print(f"Durée de la vidéo: {duration_text}")
        duration_minutes, duration_seconds = map(int, duration_text.split(":"))
        total_duration = duration_minutes * 60 + duration_seconds
        print(f"Conversion: {total_duration} secondes")
        # Attendre jusqu'à la fin de la vidéo
        time.sleep(total_duration)
        # Fermer l'onglet actuel et revenir à la liste des vidéos
        driver.close()
        driver.switch_to.window(driver.window_handles[0])

def skip_cookies(driver):
    accept_cookies_button = driver.find_element(By.XPATH, '//span[@jsname="V67aGc" and @class="VfPpkd-vQzf8d" and contains(text(), "Tout accepter")]')
    accept_cookies_button.click()
    print("Cookies acceptés")

try:
    # Ouvrir la page "Dernières nouvelles" sur YouTube
    driver.get(youtube_url)
    # Attendre que la page se charge complètement
    driver.implicitly_wait(10)
    # Rechercher la div contenant le titre "Dernières nouvelles"
    elements = driver.find_element(By.XPATH, '//span[@id="title" and contains(text(), "Dernières nouvelles")]')
    parent_div = elements.find_element(By.XPATH, './../../../../../..')
    content_div = parent_div.find_element(By.XPATH, './/div[@id="contents" and @class="style-scope ytd-rich-shelf-renderer"]')
    video_items = content_div.find_elements(By.XPATH, './*')
    selected_video_links = random.sample(video_items, min(videos_count, len(video_items)))

    video_urls = []

    for item in selected_video_links:
        video_a = item.find_element(By.XPATH, './/a[@id="thumbnail" and @class="yt-simple-endpoint inline-block style-scope ytd-thumbnail"]')
        video_urls.append(video_a.get_attribute("href"))
    # Lancer la lecture des vidéos
    watch_videos(video_urls, driver)

except:
    print("Catégorie 'Dernières nouvelles' introuvable")
    # Fermer le navigateur et recommencer depuis le début
    driver.get(youtube_url + "/channel/UCYfdidRxbB8Qhf0Nx7ioOYw")
    # Attendre que la page se charge complètement
    driver.implicitly_wait(10)
    try:
        skip_cookies(driver)
    except:
        pass
    # Trouver tous les liens de vidéos sur la page
    video_links = driver.find_elements(By.XPATH, '//a[contains(@href, "/watch?")]')
    # Sélectionner 2 vidéos au hasard parmi les liens
    selected_video_links = random.sample(video_links, min(videos_count, len(video_links)))
    # Filtrer les liens vidéo pour ignorer les vidéos en direct
    filtered_video_links = []
    for link in selected_video_links:
        try:
            # Rechercher la balise contenant le texte "EN DIRECT"
            live_indicator = link.find_element(By.XPATH, './/span[@class="style-scope ytd-thumbnail-overlay-time-status-renderer" and contains(text(), "EN DIRECT")]')
        except:
            # Si le texte "EN DIRECT" n'est pas présent, ajouter le lien à la liste filtrée
            filtered_video_links.append(link)
    # Extraire les liens href des web elements
    video_urls = [link.get_attribute("href") for link in filtered_video_links]
    # Lancer la lecture des vidéos
    watch_videos(video_urls, driver)
finally:
    driver.quit()