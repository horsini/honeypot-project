import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from pyvirtualdisplay import Display
import os
import random

def random_wait(base_wait):
    additional_wait = random.randint(0, 5)
    total_wait = base_wait + additional_wait
    time.sleep(total_wait)

def daily_task():
     # Config PyVirtualDisplay
    display = Display(visible=0, size=(1920, 1080))
    display.start()
    # Config Selenium
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--window-size=1920,1080")
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--proxy-server='direct://'")
    chrome_options.add_argument("--proxy-bypass-list=*")
    chrome_options.add_argument("--start-maximized")
    #chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--ignore-certificate-errors')
    chromedriver_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "chromedriver")
    service = Service(chromedriver_path)
    driver = webdriver.Chrome(service=service, options=chrome_options)
    driver.maximize_window()

    driver.get("https://www.google.com/")
    try:
        cookie_consent = driver.find_element(By.ID, "L2AGLb")
        cookie_consent.click()
    except pass

    search_bar = driver.find_element(By.NAME, "q")
    search_bar.send_keys("bbc")
    search_bar.send_keys(Keys.ENTER)
    random_wait(2)
    
    # Chargement de la page
    driver.get("https://www.bbc.com/")
    # Recherche de la section "module--promo"
    promo_section = driver.find_element(By.CLASS_NAME, "module--promo")
    # Comptage du nombre d'articles dans la section promo
    articles = promo_section.find_elements(By.CSS_SELECTOR, ".media-list__item")
    num_articles = len(articles)
    random_wait(5)
    # Parcourir chaque article et cliquer sur le lien
    for i in range(num_articles):
        article = promo_section.find_element(By.CSS_SELECTOR, f".media-list__item--{i+1}")
        link = article.find_element(By.CLASS_NAME, "block-link__overlay-link")
        url = link.get_attribute("href")
        driver.get(url)
        # Attendre quelques secondes pour simuler la lecture de l'article
        random_wait(10)
        # Revenir à la page précédente
        driver.execute_script("window.history.go(-1)")
        random_wait(5)
        # Chargement des éléments de la page
        promo_section = driver.find_element(By.CLASS_NAME, "module--promo")
        link = promo_section.find_elements(By.CSS_SELECTOR, ".block-link__overlay-link")

    # Fermer le navigateur
    driver.quit()
daily_task()