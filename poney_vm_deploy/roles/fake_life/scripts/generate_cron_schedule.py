#!/usr/bin/python

from crontab import CronTab
import getpass
import json
import sys
import os

# Fonction permettant de charger les profils depuis le fichier de configuration JSON 'profiles.json'
def load_profiles():
    with open('/usr/local/bin/scripts/profiles.json') as file:
        data = json.load(file)
    return data['profiles']

# Fonction permettant de récupérer la fréquence d'exécution d'un script
def generate_cron_schedule(schedule):
    minute = schedule.get('minute', '*')
    hour = schedule.get('hour', '*')
    day = schedule.get('day', '*')
    month = schedule.get('month', '*')
    day_of_week = schedule.get('day_of_week', '*')

    # On écrit la ligne du crontab définissant la fréquence d'exécution, où les valeurs par défaut sont des '*'
    cron_schedule = f"{minute} {hour} {day} {month} {day_of_week}"
    return cron_schedule

# Fonction permettant de générer les crontabs pour un profil donné
def generate_crontab(profile):

    # On itère sur les schedules du profil pour définir les différents appels d'un script
    for count, schedule in enumerate(profile['schedule'], start=1):

        # On initialise la crontab
        cron = CronTab(user=getpass.getuser())
        main_script = '/usr/bin/python3 /usr/local/bin/scripts/main.py'
        job_command = f"{main_script} {profile['name']}"
        job_comment = f"Main script execution for profile '{profile['name']}' #{count}"
        job = cron.new(
            command=job_command,
            comment=job_comment
        )

        # On récupère la fréquence d'exécution du script
        cron_schedule = generate_cron_schedule(schedule)

        # On définit la fréquence d'exécution pour la task courante
        job.setall(cron_schedule)

        # On écrit la crontab, on peut lister les crontabs avec la commande 'crontab -l'
        cron.write()

# On récupère le nom du profil à exécuter depuis les arguments, on quitte si aucun profil n'est renseigné
if len(sys.argv) < 4:
    print("Il manque des informations, aucun profil renseigné ?")
    sys.exit(1)
else:
    # Dans le cas où un profil est renseigné, on le récupère
    selected_profile_name = sys.argv[1]

# On charge les profils existants pour vérifier que le profil demandé existe
profiles = load_profiles()
selected_profile = next((p for p in profiles if p['name'].lower() == selected_profile_name.lower()), None)

# Si le profil n'existe pas, on quitte en listant les profils existants
if selected_profile is None:
    print(f"Profil '{selected_profile_name}' inexistant. Les profils disponibles sont :")
    for profile in profiles:
        print(profile['name'])
    sys.exit(1)

# Si le profil existe, on génère les crontabs pour ce dernier
generate_crontab(selected_profile)