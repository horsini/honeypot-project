#!/usr/bin/env python3

import os
import time
import socket
import paho.mqtt.client as mqtt
import datetime;

# SSH logs
log_file = "/var/log/auth.log"

# Alerte visée
password_pattern = ["Accepted password"]
sudo_vuln_pattern = ["USER=#-1", "USER=#4294967295"]
pwnkit_vuln_pattern = ["The value for the SHELL variable was not found the /etc/shells file"]
patterns = password_pattern + sudo_vuln_pattern + pwnkit_vuln_pattern
auth_alert_tracking_file = "/var/log/recent_auth_alerts.txt"
current_timestamp = int(time.time())

# MQTT Settings
broker_address="10.0.2.2"
mqtt_topic="poneypots"

def send_notification(alert_type, most_recent_alert):
    '''
        Envoie une notification de détection d'intrusion

        args:
            alert_type : Le type d'alerte à envoyer
            most_recent_alert : La dernière alerte détectée
    '''
    client = mqtt.Client("hopcheck")
    client.connect(broker_address)
    client.publish(mqtt_topic, alert_type)

def main():
    if os.path.exists(log_file):
        with open(log_file, "r") as f:
            log_lines = f.readlines()

        relevant_lines = [line for line in log_lines if (True in [x in line for x in patterns])]
        if relevant_lines:
            most_recent_alert = relevant_lines[-1]
            machine = socket.gethostname()
            ct = datetime.datetime.now()
            if True in [x in most_recent_alert for x in password_pattern]:
                alert_type = "{} SSH Authentication succeeded on {}\n{}".format(ct, machine, most_recent_alert)
            if True in [x in most_recent_alert for x in pwnkit_vuln_pattern]:
                alert_type = "{} Root access (Pwnkit) on {}\n{}".format(ct, machine, most_recent_alert)
            if True in [x in most_recent_alert for x in sudo_vuln_pattern]:
                alert_type = "{} Root access (Sudo CVE) on {}\n{}".format(ct, machine, most_recent_alert)

            if os.path.exists(auth_alert_tracking_file):
                with open(auth_alert_tracking_file, "r") as af:
                    last_alert = af.read().strip()

                if most_recent_alert.strip() != last_alert.strip():
                    send_notification(alert_type, most_recent_alert)

                    with open(auth_alert_tracking_file, "w") as af:
                        af.write(most_recent_alert)
                else:
                    print("Pas de nouvelle alerte")
            else:
                send_notification(alert_type, most_recent_alert)
                with open(auth_alert_tracking_file, "w+") as af:
                    af.write(most_recent_alert)
        else:
            print("Aucune ligne détectée")
    else:
        print("Log file not found: {}".format(log_file))

if __name__ == "__main__":
    main()
